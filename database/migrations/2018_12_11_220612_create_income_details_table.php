<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncomeDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('income_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('local_currency');
            $table->integer('total_amount');
            $table->integer('coin');
            $table->integer('voucher')->nullable();
            $table->integer('voucher_seller')->nullable();
            $table->integer('seller_rebate')->nullable();
            $table->integer('actual_shipping_cost')->nullable();
            $table->integer('shipping_fee_rebate');
            $table->integer('commission_fee');
            $table->integer('voucher_code')->nullable();
            $table->text('voucher_name')->nullable();
            $table->integer('escrow_amount');
            $table->integer('cross_border_tax')->nullable();
            $table->integer('credit_card_promotion')->nullable();
            $table->integer('credit_card_transaction_fee')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('income_details');
    }
}
