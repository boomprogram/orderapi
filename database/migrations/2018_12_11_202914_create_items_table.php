<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->string('item_id')->primary();
            $table->text('logistics');
            $table->integer('original_price');
            $table->integer('package_width')->nullable();
            $table->integer('cmt_count');
            $table->integer('weight');
            $table->integer('shopid');
            $table->string('currency');
            $table->integer('likes');
            $table->text('images')->nullable();
            $table->integer('days_to_ship');
            $table->float('package_length', 8, 2);
            $table->integer('stock');
            $table->string('status');
            $table->text('description');
            $table->integer('views');
            $table->float('price',8,2);
            $table->integer('sales');
            $table->integer('discount_id')->nullable();
            $table->text('wholesales')->nullable();
            $table->string('condition');
            $table->float('package_height',8,2);
            $table->text('name');
            $table->float('rating_star',8,2);
            $table->string('item_sku');
            $table->text('variations');
            $table->string('size_chart')->nullable();
            $table->boolean('has_variation');
            $table->text('attributes');
            $table->integer('category_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
