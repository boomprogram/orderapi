<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attributes', function (Blueprint $table) {
            $table->string('attribute_id')->primary();
            $table->text('attribute_name');
            $table->boolean('is_mandatory');
            $table->enum('attribute_type', ['INT_TYPE', 'STRING_TYPE', 'ENUM_TYPE', 'FLOAT_TYPE' , 'DATE_TYPE', 'TIMESTAMP_TYPE']);
            $table->string('attribute_value');
            $table->enum('input_type', ['DROP_DOWN', 'TEXT_FILED', 'COMBO_BOX']);
            $table->string('options');
            $table->string('values');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attributes');
    }
}
