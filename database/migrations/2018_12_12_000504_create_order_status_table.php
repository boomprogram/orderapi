<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_status', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_name');
            $table->timestamps();
        });

        $data = [
            ['id' => '1', 'order_name' => 'UNPAID'],
            ['id' => '2', 'order_name' => 'READY_TO_SHIP'],
            ['id' => '3', 'order_name' => 'RETRY_SHIP'],
            ['id' => '4', 'order_name' => 'SHIPPED'],
            ['id' => '5', 'order_name' => 'TO_CONFIRM_RECEIVE'],
            ['id' => '6', 'order_name' => 'IN_CANCEL'],
            ['id' => '7', 'order_name' => 'CANCELLED'],
            ['id' => '8', 'order_name' => 'TO_RETURN'],
            ['id' => '9', 'order_name' => 'COMPLETED']
        ];
        DB::table('order_status')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_status');
    }
}
