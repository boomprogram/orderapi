<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->integer('id_role');
            $table->integer('active');
            $table->string('portrait');
            $table->rememberToken();
            $table->timestamps();
        });

        $data = [
            'name' => 'Administrator',
            'email' => 'admin@local.com',
            'password' => Hash::make('123456'),
            'id_role' => 1,
            'active' => 1,
            'portrait' => 1
        ];

        DB::table('users')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
