<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->string('ordersn')->primary();
            $table->text('country');
            $table->text('currency');
            $table->string('cod');
            $table->string('tracking_no');
            $table->integer('days_to_ship');
            $table->text('recipient_address');
            $table->float('estimated_shipping_fee')->nullable();
            $table->float('actual_shipping_cost')->nullable();
            $table->float('total_amount');
            $table->float('escrow_amount');
            $table->string('order_status');
            $table->string('shipping_carrier');
            $table->string('payment_method');
            $table->boolean('goods_to_declare');
            $table->string('message_to_seller')->nullable();
            $table->string('note')->nullable();
            $table->string('note_update_time')->nullable();
            $table->text('items');
            $table->string('pay_time')->nullable();
            $table->string('dropshipper')->nullable();
            $table->string('buyer_username');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
