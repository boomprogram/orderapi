<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrderEscrow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE order_escrow MODIFY bank_account LONGTEXT;');
        DB::statement('ALTER TABLE order_escrow MODIFY income_details LONGTEXT;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE order_escrow MODIFY bank_account TEXT;');
        DB::statement('ALTER TABLE order_escrow MODIFY income_details TEXT;');
    }
}
