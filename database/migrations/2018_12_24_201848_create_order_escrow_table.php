<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderEscrowTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_escrow', function (Blueprint $table) {
            $table->string('ordersn')->primary();
            $table->text('activity');
            $table->integer('payee_id')->nullable();
            $table->string('shipping_carrier');
            $table->integer('exchange_rate');
            $table->text('bank_account');
            $table->text('income_details');
            $table->string('country');
            $table->string('escrow_currency');
            $table->string('escrow_channel')->nullable();
            $table->string('status');
            $table->text('items');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_escrow');
    }
}
