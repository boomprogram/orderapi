<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop', function (Blueprint $table) {
            $table->string('shop_id')->primary();
            $table->text('shop_name');
            $table->string('country');
            $table->text('shop_description');
            $table->text('videos')->nullable();
            $table->text('images')->nullable();
            $table->integer('disable_make_offer');
            $table->integer('enable_display_unitno');
            $table->integer('item_limit');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop');
    }
}
