<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReturnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('returns', function (Blueprint $table) {
            $table->increments('id');
            $table->text('images');
            $table->enum('reason', [
                            'NONE',
                            'NOT_RECEIPT',
                            'WRONG_ITEM',
                            'ITEM_DAMAGED',
                            'DIFFERENT_DESCRIPTION',
                            'MUTUAL_AGREE',
                            'OTHER',
                            'ITEM_WRONGDAMAGED(only for Vietnam)',
                            'CHANGE_MIND',
                            'ITEM_MISSING',
                            'EXPECTATION_FAILED',
                            'ITEM_FAKE',
                            ]);
            $table->string('text_reason');
            $table->string('returnsn');
            $table->float('refund_amount');
            $table->string('currency');
            $table->enum('status', [
                            'REQUESTED',
                            'ACCEPTED',
                            'CANCELLED',
                            'JUDGING',
                            'REFUND_PAID',
                            'CLOSED',
                            'PROCESSING',
                            'SELLER_DISPUTE'
                            ]);
            $table->dateTime('due_date');
            $table->string('tracking_number');
            $table->enum('dispute_reason',['NON_RECEIPT', 'OTHER', 'NOT_RECEIVED', 'UNKNOWN']);
            $table->string('dispute_text_reason');
            $table->boolean('needs_logistics');
            $table->float('amount_before_discount', 8, 2);
            $table->integer('user');
            $table->integer('items');
            $table->string('ordersn');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('returns');
    }
}
