<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logistics', function (Blueprint $table) {
            $table->string('logistic_id')->primary();
            $table->text('logistic_name');
            $table->boolean('enabled');
            $table->float('shipping_fee',8,2);
            $table->integer('size_id');
            $table->boolean('is_free')->nullable();
            $table->float('estimated_shipping_fee',8,2);
            $table->boolean('has_cod');
            $table->integer('item_max_dimension')->nullable();
            $table->integer('sizes');
            $table->enum('fee_type', ['SIZE_SELECTION', 'SIZE_INPUT', 'FIXED_DEFAULT_PRICE', 'CUSTOM_PRICE']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logistics');
    }
}
