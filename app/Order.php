<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use \Staudenmeir\EloquentJsonRelations\HasJsonRelationships;

    public $incrementing = false;
    protected $primaryKey = "ordersn";
    protected $table = "orders";
    protected $fillable = [
                'activity',
                'payee_id',
                'shipping_carrier',
                'exchange_rate',
                'bank_account',
                'income_details',
                'country',
                'escrow_currency',
                'escrow_channel',
                'items'
            ];
    protected $casts = [
        'items' => 'array',
    ];
    public function item()
    {
        return $this->belongsTo(Product::Class, 'items->item_id', 'item_id');
    }
}
