<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shop;
use App\Order;
use Shopee;
use Illuminate\Support\Facades\DB;
use App\OrderStatus;

class ConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shops = Shop::get();

        return view('config.shop', compact('shops'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function testdata()
    {
        $client = new Shopee\Client([
            'secret' => '1f30a88d84b5e4d2309307e799ff20079a4b30163c5e3d652203f0386eeb266e',
            'partner_id' => 840997,
            'shopid' => 52591873,
        ]);
        $getOrderPaid = $client->order->getOrdersByStatus(['order_status' => 'COMPLETED']);
        $dataOrderPaid = $getOrderPaid->getData();
        $arr_order = array();
        $OrderSn = array();
        $IDOrder = array();
        $updateData = [];
        foreach($dataOrderPaid['orders'] as $order){
            $OrderSn[] = $order['ordersn'];
            $IDOrder[] = '"'.$order['ordersn'].'"';
        }
        $getOrderDetails = $client->order->getOrderDetails(['ordersn_list' => $OrderSn]);
        $dataOrderDetails = $getOrderDetails->getData();
        foreach($dataOrderDetails['orders'] as $k => $order){
            $ordersn = $order['ordersn']; //1
            $country = $order['country']; //2
            $currency = $order['currency']; //3
            $cod = $order['cod']; //4
            $tracking_no = $order['tracking_no']; //5
            $days_to_ship = $order['days_to_ship']; //6
            $recipient_address = $order['recipient_address']; //7
                $arr_address = array(
                    'town' => $recipient_address['town'],
                    'city' => $recipient_address['city'],
                    'name' => $recipient_address['name'],
                    'district' => $recipient_address['district'],
                    'country' => $recipient_address['country'],
                    'zipcode' => $recipient_address['zipcode'],
                    'full_address' => $recipient_address['full_address'],
                    'phone' => $recipient_address['phone'],
                    'state' => $recipient_address['state']
                );
                $json_address = json_encode($arr_address,JSON_UNESCAPED_UNICODE);
            $estimated_shipping_fee = $order['estimated_shipping_fee']; //8
            $actual_shipping_cost = $order['actual_shipping_cost']; //9
            $total_amount = $order['total_amount']; //10
            $escrow_amount = $order['escrow_amount']; //11
            $order_status = $order['order_status']; //12
            $shipping_carrier = $order['shipping_carrier']; //13
            $payment_method = $order['payment_method']; //14
            $goods_to_declare = $order['goods_to_declare']; //15
            $message_to_seller = $order['message_to_seller']; //16
            $note = $order['note']; //17
            $note_update_time = $order['note_update_time']; //18
            $items = $order['items']; //19
            $arr_item = NULL;
            foreach($items as $item){
                $arr_item = array(
                    'item_id' => $item['item_id'],
                    'item_name' => $item['item_name'],
                    'item_sku' => $item['item_sku'],
                    'variation_id' => $item['variation_id'],
                    'variation_name' => $item['variation_name'],
                    'variation_sku' => $item['variation_sku'],
                    'variation_quantity_purchased' => $item['variation_quantity_purchased'],
                    'variation_original_price' => $item['variation_original_price'],
                    'variation_discounted_price' => $item['variation_discounted_price'],
                    'is_wholesale' => $item['is_wholesale'],
                );
            }
            $jsonItems = json_encode($arr_item,JSON_UNESCAPED_UNICODE);
            $pay_time = $order['pay_time']; //20
            $dropshipper = $order['dropshipper']; //21
            $buyer_username = $order['buyer_username']; //22
            $create_time = date('Y-m-d H:i:s',$order['create_time']); //23
            $update_time = date('Y-m-d H:i:s',$order['update_time']); //24
            
            $updateData = [
                    'ordersn' => $ordersn,
                    'tracking_no' => $tracking_no,
                    'order_status' => $order_status,
                    'updated_at' => $update_time
                ];
            DB::table('orders')->where('ordersn', $ordersn)->update($updateData);
            //$values = Order::where('id', $ordersn)->update(['update'=>$update]);
            $cols[] = 'UPDATE `orders` SET `tracking_no`="'.$tracking_no.'", `order_status`="'.$order_status.'" ,`updated_at`="'.$update_time.'" where ordersn = "'.$ordersn.'" ';

            //$update_order[] = 'UPDATE `orders` SET '.$cols.' ';

            $arr_order[] = "('".$ordersn."', '".$country."', '".$currency."', '".$cod."', '".$tracking_no."', '".$days_to_ship."', '".$json_address."', '".$estimated_shipping_fee."', '".$actual_shipping_cost."', '".$total_amount."', '".$escrow_amount."', '".$order_status."', '".$shipping_carrier."', '".$payment_method."', '".$goods_to_declare."', '".$message_to_seller."', '".$note."', '".$note_update_time."', '".$jsonItems."', '".$pay_time."', '".$dropshipper."', '".$buyer_username."', '".$create_time."', '".$update_time."')";
        }
        //dd($updateData);     
        //$implodeArray = implode('; ', $cols);
        //dd($implodeArray);
        DB::insert('INSERT IGNORE INTO `orders` (`ordersn`, `country`, `currency`, `cod`, `tracking_no`, `days_to_ship`, `recipient_address`, `estimated_shipping_fee`, `actual_shipping_cost`, `total_amount`, `escrow_amount`, `order_status`, `shipping_carrier`, `payment_method`, `goods_to_declare`, `message_to_seller`, `note`, `note_update_time`, `items`, `pay_time`, `dropshipper`, `buyer_username`, `created_at`, `updated_at`) VALUES '.implode(',',$arr_order).';');
        //DB::statement($implodeArray);
        
    }
    public function syncdata(Request $request)
    {
        $client = new Shopee\Client([
            'secret' => $request->get('secret'),
            'partner_id' => (int)$request->get('partner_id'),
            'shopid' => (int)$request->get('shopid'),
        ]);
        $client = new Shopee\Client([
            'secret' => '1f30a88d84b5e4d2309307e799ff20079a4b30163c5e3d652203f0386eeb266e',
            'partner_id' => 840997,
            'shopid' => 52591873,
        ]);
        // //ShopName//
        // $getShop = $client->shop->getShopInfo();
        // $dataShop = $getShop->getData();
        // //dd($dataShop);
        //     $shop_id = $dataShop['shop_id'];
        //     $shop_name = $dataShop['shop_name'];
        //     $country = $dataShop['country'];
        //     $shop_description = $dataShop['shop_description'];

        //     $videos_shop = $dataShop['videos'];
        //         $arr_videoShop = NULL;
        //         if(is_array($videos_shop)){
        //             foreach($videos_shop as $videoShop){
        //                 $arr_videoShop = $videoShop;
        //             }
        //         }

        //     $images_shop = $dataShop['images'];
        //         $arr_imgShop = NULL;
        //         if(is_array($images_shop)){
        //             foreach($images_shop as $imgShop){
        //                 $arr_imgShop = $imgShop;
        //             }
        //         }

        //     $disable_make_offer = $dataShop['disable_make_offer'];
        //     $enable_display_unitno = $dataShop['enable_display_unitno'];
        //     $item_limit = $dataShop['item_limit'];
        //     $status = $dataShop['status'];

        //     foreach($dataShop as $shop){
        //         $arr_Shop = '("'.$shop_id.'", "'.$shop_name.'", "'.$country.'", "'.$shop_description.'", "'.$arr_videoShop.'", "'.$arr_imgShop.'", "'.$disable_make_offer.'", "'.$enable_display_unitno.'", "'.$item_limit.'", "'.$status.'")';
        //     }
        //     DB::insert('INSERT IGNORE INTO `shop`(`shop_id`, `shop_name`, `country`, `shop_description`, `videos`, `images`, `disable_make_offer`, `enable_display_unitno`, `item_limit`, `status`) VALUES '.$arr_Shop.';');
        // //Categories//
        // $getCat = $client->item->getCategories();
        // $dataCat = $getCat->getData();

        // $arr_dataCat = array();
        // foreach ($dataCat['categories'] as $row) {
        //     $arr_dataCat[] = '("'.$row['category_id'].'", "'.$row['parent_id'].'", "'.$row['category_name'].'", "'.$row['has_children'].'")';
        // }
        // DB::insert('INSERT IGNORE INTO categories (category_id, parent_id, category_name, has_children) VALUES '.implode(',',$arr_dataCat).';');
        // //Address_list//
        // $getAddress = $client->logistics->getAddress();
        // $dataAddress = $getAddress->getData();
        // $arr_dataAddress = array();
        // foreach ($dataAddress['address_list'] as $row) {
        //     $arr_dataAddress[] = '("'.$row['address_id'].'", "'.$row['country'].'", "'.$row['state'].'", "'.$row['city'].'", "'.$row['address'].'", "'.$row['zipcode'].'", "'.$row['district'].'", "'.$row['town'].'")';
        // }
        // DB::insert('INSERT IGNORE INTO address (address_id, country, state, city, address, zipcode, district, town) VALUES '.implode(',',$arr_dataAddress).';');
        // //TimeSlot//
        // $getPayment = $client->payments->getPaymentList();
        // $dataPayment = $getPayment->getData();
        // $arr_dataPayment = array();
        // foreach ($dataPayment['payment_method_list'] as $row) {
        //     $arr_dataPayment[] = '("'.$row['payment_method'].'", "'.$row['country'].'")';
        // }
        // DB::insert('INSERT IGNORE INTO payments (payment_method, country) VALUES '.implode(',',$arr_dataPayment).';');
        // //Discount//
        // $getDiscount = $client->discount->getDiscountsList(['discount_status' => 'ALL', 'pagination_offset' => 0 , 'pagination_entries_per_page' => 100]);
        // $dataDiscount = $getDiscount->getData();
        // $arr_discount = array();
        // foreach ($dataDiscount['discount'] as $discount){
        //     $arr_discount[] = '("'.$discount['discount_id'].'", "'.$discount['discount_name'].'", "'.date('Y-m-d H:i:s',$discount['start_time']).'", "'.date('Y-m-d H:i:s',$discount['end_time']).'")';
        // }
        // DB::insert('INSERT IGNORE INTO discount (discount_id, discount_name, start_time, end_time) VALUES '.implode(',',$arr_discount).';');

        // $getItems = $client->item->getItemsList(['pagination_offset' => 0, 'pagination_entries_per_page' => 100]);
        // $dataItems = $getItems->getData();
        // $arr_items = array();
        // foreach ($dataItems['items'] as $rows) {
        //     $itemId = $rows['item_id'].",";
        //     $ex_Id = explode(',', $itemId);
        //     $item_id = $ex_Id[0];

        //     $response = $client->item->GetItemDetail(['item_id' => (int)$ex_Id[0]]);
        //     $data = $response->getData();
        //     //dd($data);
        //     $item_id = $data['item']['item_id'];
        //     $logistics = $data['item']['logistics'];
        //     //Logistice//
        //     foreach($logistics as $logistic){

        //         $logistics_id = $logistic['logistic_id'];
        //         $logistics_name = $logistic['logistic_name'];
        //         $enabled = $logistic['enabled'];
        //         $is_free = $logistic['is_free'];

        //         DB::insert('INSERT IGNORE INTO logistics (logistic_id, logistic_name, enabled, is_free, estimated_shipping_fee) VALUES ("'.$logistics_id.'", "'.$logistics_name.'", "'.$enabled.'", "'.$is_free.'", 1)');
        //     }
        //     //Logistice//
        //     $original_price = $data['item']['original_price'];
        //     $package_width = $data['item']['package_width'];
        //     $cmt_count = $data['item']['cmt_count'];
        //     $weight = $data['item']['weight'];
        //     $shopid = $data['item']['shopid'];
        //     $currency = $data['item']['currency'];
        //     $likes = $data['item']['likes'];
        //     $images = $data['item']['images'];
        //     //Images//
        //     $arr_Image = array();
        //     foreach($images as $image){
        //         $arr_Image[] = $image;
        //     }
        //     DB::insert('INSERT IGNORE INTO images (image, item_id) VALUES ("'.implode(',',$arr_Image).'", "'.$item_id.'")');

        //     $jsonImg = json_encode($arr_Image,JSON_UNESCAPED_UNICODE);
        //     //Images//
        //     $days_to_ship = $data['item']['days_to_ship'];
        //     $package_length = $data['item']['package_length'];
        //     $stock = $data['item']['stock'];
        //     $status = $data['item']['status'];
        //     $description = $data['item']['description'];
        //     $description = str_replace("\n", "<br/>", $description);
        //     $description = str_replace("\t", "&nbsp;", $description);
        //     $description = str_replace('"', " ", $description);
        //     $views = $data['item']['views'];
        //     $price = $data['item']['price'];
        //     $sales = $data['item']['sales'];
        //     $discount_id = $data['item']['discount_id'];
        //     $wholesales = $data['item']['wholesales'];

        //     $arr_wholesales = NULL;
        //     foreach($wholesales as $wholesale){
        //         if(is_array($wholesales)){
        //             $arr_wholesales = array(
        //                 'min' => $wholesale['min'],
        //                 'max' => $wholesale['max'],
        //                 'unit_price' => $wholesale['unit_price']
        //             );
        //         }
        //     }
        //     $json_whole = json_encode($arr_wholesales,JSON_UNESCAPED_UNICODE);

        //     $condition = $data['item']['condition'];
        //     $package_height = $data['item']['package_height'];
        //     $name = $data['item']['name'];
        //     $rating_star = $data['item']['rating_star'];
        //     $item_sku = $data['item']['item_sku'];
        //     $variations = $data['item']['variations'];

        //     $arr_variations = NULL;
        //     foreach($variations as $variation){
        //         if(is_array($variations)){
        //             $arr_variations = array(
        //                 'variation_id' => $variation['variation_id'],
        //                 'variation_sku' => $variation['variation_sku'],
        //                 'name' => $variation['name'],
        //                 'price' => $variation['price'],
        //                 'stock' => $variation['stock'],
        //                 'status' => $variation['status'],
        //                 'original_price' => $variation['original_price']
        //             );
        //         }
        //     }
        //     $json_variations = json_encode($arr_variations,JSON_UNESCAPED_UNICODE);
        //     //DB::insert('INSERT IGNORE INTO variations (variation_id, variation_sku, name, price, stock, status, original_price) VALUES '.implode(',',$arr_variations).';');

        //     $size_chart = $data['item']['size_chart'];
        //     $has_variation = $data['item']['has_variation'];

        //     $attributes = $data['item']['attributes'];
        //     $arr_attributes = NULL;
        //     foreach($attributes as $attribute){
        //         if(is_array($attributes)){
        //             $arr_attributes = array(
        //                 'attribute_id' => $attribute['attribute_id'],
        //                 'attribute_name' => $attribute['attribute_name'],
        //                 'is_mandatory' => $attribute['is_mandatory'],
        //                 'attribute_type' => $attribute['attribute_type'],
        //                 'attribute_value' => $attribute['attribute_value']
        //             );
        //         }
        //     }
        //     $json_attribute = json_encode($arr_attributes,JSON_UNESCAPED_UNICODE);

        //     $category_id = $data['item']['category_id'];
        //     $create_time = date('Y-m-d H:i:s',$data['item']['create_time']); //23
        //     $update_time = date('Y-m-d H:i:s',$data['item']['update_time']); //24

        //     $arr_items[] = "('".$item_id."', '".$logistics_id."', '".$original_price."', '".$package_width."', '".$cmt_count."', '".$weight."', '".$shopid."', '".$currency."', '".$likes."', '".$jsonImg."', '".$days_to_ship."', '".$package_length."', '".$stock."', '".$status."', '".$description."', '".$views."', '".$price."', '".$sales."', '".$discount_id."', '".$json_whole."', '".$condition."', '".$package_height."', '".$name."', '".$rating_star."','".$item_sku."', '".$json_variations."', '".$size_chart."', '".$has_variation."', '".$json_attribute."', '".$category_id."', '".$create_time."', '".$update_time."')";
        // }

        // DB::insert('INSERT IGNORE INTO `items` (`item_id`, `logistics`, `original_price`, `package_width`, `cmt_count`, `weight`, `shopid`, `currency`, `likes`, `images`, `days_to_ship`, `package_length`, `stock`, `status`, `description`, `views`, `price`, `sales`, `discount_id`, `wholesales`, `condition`, `package_height`, `name`, `rating_star`, `item_sku`, `variations`, `size_chart`, `has_variation`, `attributes`, `category_id`, `created_at`, `updated_at`) VALUES '.implode(',',$arr_items).'; ');
        //Order//------------------------------------------------------//Order//
        $getOrderPaid = $client->order->getOrdersByStatus(['order_status' => 'UNPAID']);
        $dataOrderPaid = $getOrderPaid->getData();
        //dd($getOrderPaid);
        $arr_order = array();
        $OrderSn = array();
        foreach($dataOrderPaid['orders'] as $order){
            $OrderSn[] = $order['ordersn'];
        }
        $getOrderDetails = $client->order->getOrderDetails(['ordersn_list' => $OrderSn]);
        $dataOrderDetails = $getOrderDetails->getData();
        foreach($dataOrderDetails['orders'] as $order){
            $ordersn = $order['ordersn']; //1
            $country = $order['country']; //2
            $currency = $order['currency']; //3
            $cod = $order['cod']; //4
            $tracking_no = $order['tracking_no']; //5
            $days_to_ship = $order['days_to_ship']; //6
            $recipient_address = $order['recipient_address']; //7
                $arr_address = array(
                    'town' => $recipient_address['town'],
                    'city' => $recipient_address['city'],
                    'name' => $recipient_address['name'],
                    'district' => $recipient_address['district'],
                    'country' => $recipient_address['country'],
                    'zipcode' => $recipient_address['zipcode'],
                    'full_address' => $recipient_address['full_address'],
                    'phone' => $recipient_address['phone'],
                    'state' => $recipient_address['state']
                );
                $json_address = json_encode($arr_address,JSON_UNESCAPED_UNICODE);
            $estimated_shipping_fee = $order['estimated_shipping_fee']; //8
            $actual_shipping_cost = $order['actual_shipping_cost']; //9
            $total_amount = $order['total_amount']; //10
            $escrow_amount = $order['escrow_amount']; //11
            $order_status = $order['order_status']; //12
            $shipping_carrier = $order['shipping_carrier']; //13
            $payment_method = $order['payment_method']; //14
            $goods_to_declare = $order['goods_to_declare']; //15
            $message_to_seller = $order['message_to_seller']; //16
            $note = $order['note']; //17
            $note_update_time = $order['note_update_time']; //18
            $items = $order['items']; //19
            $arr_item = NULL;
            foreach($items as $item){
                $arr_item = array(
                    'item_id' => $item['item_id'],
                    'item_name' => $item['item_name'],
                    'item_sku' => $item['item_sku'],
                    'variation_id' => $item['variation_id'],
                    'variation_name' => $item['variation_name'],
                    'variation_sku' => $item['variation_sku'],
                    'variation_quantity_purchased' => $item['variation_quantity_purchased'],
                    'variation_original_price' => $item['variation_original_price'],
                    'variation_discounted_price' => $item['variation_discounted_price'],
                    'is_wholesale' => $item['is_wholesale'],
                );
            }
            $jsonItems = json_encode($arr_item,JSON_UNESCAPED_UNICODE);
            $pay_time = $order['pay_time']; //20
            $dropshipper = $order['dropshipper']; //21
            $buyer_username = $order['buyer_username']; //22
            $create_time = date('Y-m-d H:i:s',$order['create_time']); //23
            $update_time = date('Y-m-d H:i:s',$order['update_time']); //24


            $updateData = [
                'ordersn' => $ordersn,
                'tracking_no' => $tracking_no,
                'order_status' => $order_status,
                'updated_at' => $update_time
            ];
            DB::table('orders')->where('ordersn', $ordersn)->update($updateData);

            $cols[] = 'UPDATE `orders` SET `tracking_no`="'.$tracking_no.'", `order_status`="'.$order_status.'" ,`updated_at`="'.$update_time.'" where ordersn = "'.$ordersn.'" ';

            $arr_order[] = "('".$ordersn."', '".$country."', '".$currency."', '".$cod."', '".$tracking_no."', '".$days_to_ship."', '".$json_address."', '".$estimated_shipping_fee."', '".$actual_shipping_cost."', '".$total_amount."', '".$escrow_amount."', '".$order_status."', '".$shipping_carrier."', '".$payment_method."', '".$goods_to_declare."', '".$message_to_seller."', '".$note."', '".$note_update_time."', '".$jsonItems."', '".$pay_time."', '".$dropshipper."', '".$buyer_username."', '".$create_time."', '".$update_time."')";
        }
        DB::insert('INSERT IGNORE INTO `orders` (`ordersn`, `country`, `currency`, `cod`, `tracking_no`, `days_to_ship`, `recipient_address`, `estimated_shipping_fee`, `actual_shipping_cost`, `total_amount`, `escrow_amount`, `order_status`, `shipping_carrier`, `payment_method`, `goods_to_declare`, `message_to_seller`, `note`, `note_update_time`, `items`, `pay_time`, `dropshipper`, `buyer_username`, `created_at`, `updated_at`) VALUES '.implode(',',$arr_order).';');
        //$implodeArray = implode("; ", $cols);
        //DB::statement($implodeArray);


        $getOrderReady = $client->order->getOrdersByStatus(['order_status' => 'READY_TO_SHIP']);
        $dataOrderReady = $getOrderReady->getData();
        //dd($dataOrderReady);
        $arr_order = array();
        $OrderSn = array();
        foreach($dataOrderReady['orders'] as $order){
            $OrderSn[] = $order['ordersn'];
        }
        $getOrderDetails = $client->order->getOrderDetails(['ordersn_list' => $OrderSn]);
        $dataOrderDetails = $getOrderDetails->getData();
        foreach($dataOrderDetails['orders'] as $order){
            $ordersn = $order['ordersn']; //1
            $country = $order['country']; //2
            $currency = $order['currency']; //3
            $cod = $order['cod']; //4
            $tracking_no = $order['tracking_no']; //5
            $days_to_ship = $order['days_to_ship']; //6
            $recipient_address = $order['recipient_address']; //7
                $arr_address = array(
                    'town' => $recipient_address['town'],
                    'city' => $recipient_address['city'],
                    'name' => $recipient_address['name'],
                    'district' => $recipient_address['district'],
                    'country' => $recipient_address['country'],
                    'zipcode' => $recipient_address['zipcode'],
                    'full_address' => $recipient_address['full_address'],
                    'phone' => $recipient_address['phone'],
                    'state' => $recipient_address['state']
                );
                $json_address = json_encode($arr_address,JSON_UNESCAPED_UNICODE);
            $estimated_shipping_fee = $order['estimated_shipping_fee']; //8
            $actual_shipping_cost = $order['actual_shipping_cost']; //9
            $total_amount = $order['total_amount']; //10
            $escrow_amount = $order['escrow_amount']; //11
            $order_status = $order['order_status']; //12
            $shipping_carrier = $order['shipping_carrier']; //13
            $payment_method = $order['payment_method']; //14
            $goods_to_declare = $order['goods_to_declare']; //15
            $message_to_seller = $order['message_to_seller']; //16
            $note = $order['note']; //17
            $note_update_time = $order['note_update_time']; //18
            $items = $order['items']; //19
            $arr_item = NULL;
            foreach($items as $item){
                $arr_item = array(
                    'item_id' => $item['item_id'],
                    'item_name' => $item['item_name'],
                    'item_sku' => $item['item_sku'],
                    'variation_id' => $item['variation_id'],
                    'variation_name' => $item['variation_name'],
                    'variation_sku' => $item['variation_sku'],
                    'variation_quantity_purchased' => $item['variation_quantity_purchased'],
                    'variation_original_price' => $item['variation_original_price'],
                    'variation_discounted_price' => $item['variation_discounted_price'],
                    'is_wholesale' => $item['is_wholesale'],
                );
            }
            $jsonItems = json_encode($arr_item,JSON_UNESCAPED_UNICODE);
            $pay_time = $order['pay_time']; //20
            $dropshipper = $order['dropshipper']; //21
            $buyer_username = $order['buyer_username']; //22
            $create_time = date('Y-m-d H:i:s',$order['create_time']); //23
            $update_time = date('Y-m-d H:i:s',$order['update_time']); //24

            $updateData = [
                'ordersn' => $ordersn,
                'tracking_no' => $tracking_no,
                'order_status' => $order_status,
                'updated_at' => $update_time
            ];
            DB::table('orders')->where('ordersn', $ordersn)->update($updateData);

            $cols[] = 'UPDATE `orders` SET `tracking_no`="'.$tracking_no.'", `order_status`="'.$order_status.'" ,`updated_at`="'.$update_time.'" where ordersn = "'.$ordersn.'" ';

            $arr_order[] = "('".$ordersn."', '".$country."', '".$currency."', '".$cod."', '".$tracking_no."', '".$days_to_ship."', '".$json_address."', '".$estimated_shipping_fee."', '".$actual_shipping_cost."', '".$total_amount."', '".$escrow_amount."', '".$order_status."', '".$shipping_carrier."', '".$payment_method."', '".$goods_to_declare."', '".$message_to_seller."', '".$note."', '".$note_update_time."', '".$jsonItems."', '".$pay_time."', '".$dropshipper."', '".$buyer_username."', '".$create_time."', '".$update_time."')";
        }
        DB::insert('INSERT IGNORE INTO `orders` (`ordersn`, `country`, `currency`, `cod`, `tracking_no`, `days_to_ship`, `recipient_address`, `estimated_shipping_fee`, `actual_shipping_cost`, `total_amount`, `escrow_amount`, `order_status`, `shipping_carrier`, `payment_method`, `goods_to_declare`, `message_to_seller`, `note`, `note_update_time`, `items`, `pay_time`, `dropshipper`, `buyer_username`, `created_at`, `updated_at`) VALUES '.implode(',',$arr_order).';');
        //$implodeArray = implode("; ", $cols);
        //DB::statement($implodeArray);

        $getOrderComplete = $client->order->getOrdersByStatus(['order_status' => 'COMPLETED']);
        $dataOrderComplete = $getOrderComplete->getData();
        $arr_order = array();
        $OrderSn = array();
        foreach($dataOrderComplete['orders'] as $order){
            $OrderSn[] = $order['ordersn'];
        }
        $getOrderDetails = $client->order->getOrderDetails(['ordersn_list' => $OrderSn]);
        $dataOrderDetails = $getOrderDetails->getData();
        foreach($dataOrderDetails['orders'] as $order){
            $ordersn = $order['ordersn']; //1
            $country = $order['country']; //2
            $currency = $order['currency']; //3
            $cod = $order['cod']; //4
            $tracking_no = $order['tracking_no']; //5
            $days_to_ship = $order['days_to_ship']; //6
            $recipient_address = $order['recipient_address']; //7
                $arr_address = array(
                    'town' => $recipient_address['town'],
                    'city' => $recipient_address['city'],
                    'name' => $recipient_address['name'],
                    'district' => $recipient_address['district'],
                    'country' => $recipient_address['country'],
                    'zipcode' => $recipient_address['zipcode'],
                    'full_address' => $recipient_address['full_address'],
                    'phone' => $recipient_address['phone'],
                    'state' => $recipient_address['state']
                );
                $json_address = json_encode($arr_address,JSON_UNESCAPED_UNICODE);
            $estimated_shipping_fee = $order['estimated_shipping_fee']; //8
            $actual_shipping_cost = $order['actual_shipping_cost']; //9
            $total_amount = $order['total_amount']; //10
            $escrow_amount = $order['escrow_amount']; //11
            $order_status = $order['order_status']; //12
            $shipping_carrier = $order['shipping_carrier']; //13
            $payment_method = $order['payment_method']; //14
            $goods_to_declare = $order['goods_to_declare']; //15
            $message_to_seller = $order['message_to_seller']; //16
            $note = $order['note']; //17
            $note_update_time = $order['note_update_time']; //18
            $items = $order['items']; //19
            $arr_item = NULL;
            foreach($items as $item){
                $arr_item = array(
                    'item_id' => $item['item_id'],
                    'item_name' => $item['item_name'],
                    'item_sku' => $item['item_sku'],
                    'variation_id' => $item['variation_id'],
                    'variation_name' => $item['variation_name'],
                    'variation_sku' => $item['variation_sku'],
                    'variation_quantity_purchased' => $item['variation_quantity_purchased'],
                    'variation_original_price' => $item['variation_original_price'],
                    'variation_discounted_price' => $item['variation_discounted_price'],
                    'is_wholesale' => $item['is_wholesale'],
                );
            }
            $jsonItems = json_encode($arr_item,JSON_UNESCAPED_UNICODE);
            $pay_time = $order['pay_time']; //20
            $dropshipper = $order['dropshipper']; //21
            $buyer_username = $order['buyer_username']; //22
            $create_time = date('Y-m-d H:i:s',$order['create_time']); //23
            $update_time = date('Y-m-d H:i:s',$order['update_time']); //24

            $updateData = [
                'ordersn' => $ordersn,
                'tracking_no' => $tracking_no,
                'order_status' => $order_status,
                'updated_at' => $update_time
            ];
            DB::table('orders')->where('ordersn', $ordersn)->update($updateData);

            $cols[] = 'UPDATE `orders` SET `tracking_no`="'.$tracking_no.'", `order_status`="'.$order_status.'" ,`updated_at`="'.$update_time.'" where ordersn = "'.$ordersn.'" ';

            $arr_order[] = "('".$ordersn."', '".$country."', '".$currency."', '".$cod."', '".$tracking_no."', '".$days_to_ship."', '".$json_address."', '".$estimated_shipping_fee."', '".$actual_shipping_cost."', '".$total_amount."', '".$escrow_amount."', '".$order_status."', '".$shipping_carrier."', '".$payment_method."', '".$goods_to_declare."', '".$message_to_seller."', '".$note."', '".$note_update_time."', '".$jsonItems."', '".$pay_time."', '".$dropshipper."', '".$buyer_username."', '".$create_time."', '".$update_time."')";
        }
        DB::insert('INSERT IGNORE INTO `orders` (`ordersn`, `country`, `currency`, `cod`, `tracking_no`, `days_to_ship`, `recipient_address`, `estimated_shipping_fee`, `actual_shipping_cost`, `total_amount`, `escrow_amount`, `order_status`, `shipping_carrier`, `payment_method`, `goods_to_declare`, `message_to_seller`, `note`, `note_update_time`, `items`, `pay_time`, `dropshipper`, `buyer_username`, `created_at`, `updated_at`) VALUES '.implode(',',$arr_order).';');
        //$implodeArray = implode("; ", $cols);
        //DB::statement($implodeArray);

        $getOrderPaid = $client->order->getOrdersByStatus(['order_status' => 'IN_CANCEL']);
        $dataOrderPaid = $getOrderPaid->getData();
        $arr_order = array();
        $OrderSn = array();
        if(is_array($dataOrderPaid['orders'])){
            foreach($dataOrderPaid['orders'] as $order){
                $OrderSn[] = $order['ordersn'];
            }
        }
        $getOrderDetails = $client->order->getOrderDetails(['ordersn_list' => $OrderSn]);
        $dataOrderDetails = $getOrderDetails->getData();
        if(!empty($dataOrderDetails['orders'])){
            foreach($dataOrderDetails['orders'] as $order){
                $ordersn = $order['ordersn']; //1
                $country = $order['country']; //2
                $currency = $order['currency']; //3
                $cod = $order['cod']; //4
                $tracking_no = $order['tracking_no']; //5
                $days_to_ship = $order['days_to_ship']; //6
                $recipient_address = $order['recipient_address']; //7
                    $arr_address = array(
                        'town' => $recipient_address['town'],
                        'city' => $recipient_address['city'],
                        'name' => $recipient_address['name'],
                        'district' => $recipient_address['district'],
                        'country' => $recipient_address['country'],
                        'zipcode' => $recipient_address['zipcode'],
                        'full_address' => $recipient_address['full_address'],
                        'phone' => $recipient_address['phone'],
                        'state' => $recipient_address['state']
                    );
                    $json_address = json_encode($arr_address,JSON_UNESCAPED_UNICODE);
                $estimated_shipping_fee = $order['estimated_shipping_fee']; //8
                $actual_shipping_cost = $order['actual_shipping_cost']; //9
                $total_amount = $order['total_amount']; //10
                $escrow_amount = $order['escrow_amount']; //11
                $order_status = $order['order_status']; //12
                $shipping_carrier = $order['shipping_carrier']; //13
                $payment_method = $order['payment_method']; //14
                $goods_to_declare = $order['goods_to_declare']; //15
                $message_to_seller = $order['message_to_seller']; //16
                $note = $order['note']; //17
                $note_update_time = $order['note_update_time']; //18
                $items = $order['items']; //19
                $arr_item = NULL;
                foreach($items as $item){
                    $arr_item = array(
                        'item_id' => $item['item_id'],
                        'item_name' => $item['item_name'],
                        'item_sku' => $item['item_sku'],
                        'variation_id' => $item['variation_id'],
                        'variation_name' => $item['variation_name'],
                        'variation_sku' => $item['variation_sku'],
                        'variation_quantity_purchased' => $item['variation_quantity_purchased'],
                        'variation_original_price' => $item['variation_original_price'],
                        'variation_discounted_price' => $item['variation_discounted_price'],
                        'is_wholesale' => $item['is_wholesale'],
                    );
                }
                $jsonItems = json_encode($arr_item,JSON_UNESCAPED_UNICODE);
                $pay_time = $order['pay_time']; //20
                $dropshipper = $order['dropshipper']; //21
                $buyer_username = $order['buyer_username']; //22
                $create_time = date('Y-m-d H:i:s',$order['create_time']); //23
                $update_time = date('Y-m-d H:i:s',$order['update_time']); //24

                $updateData = [
                    'ordersn' => $ordersn,
                    'tracking_no' => $tracking_no,
                    'order_status' => $order_status,
                    'updated_at' => $update_time
                ];
                DB::table('orders')->where('ordersn', $ordersn)->update($updateData);

                $cols[] = 'UPDATE `orders` SET `tracking_no`="'.$tracking_no.'", `order_status`="'.$order_status.'" ,`updated_at`="'.$update_time.'" where ordersn = "'.$ordersn.'" ';
                
                $arr_order[] = "('".$ordersn."', '".$country."', '".$currency."', '".$cod."', '".$tracking_no."', '".$days_to_ship."', '".$json_address."', '".$estimated_shipping_fee."', '".$actual_shipping_cost."', '".$total_amount."', '".$escrow_amount."', '".$order_status."', '".$shipping_carrier."', '".$payment_method."', '".$goods_to_declare."', '".$message_to_seller."', '".$note."', '".$note_update_time."', '".$jsonItems."', '".$pay_time."', '".$dropshipper."', '".$buyer_username."', '".$create_time."', '".$update_time."')";
            }

            DB::insert('INSERT IGNORE INTO `orders` (`ordersn`, `country`, `currency`, `cod`, `tracking_no`, `days_to_ship`, `recipient_address`, `estimated_shipping_fee`, `actual_shipping_cost`, `total_amount`, `escrow_amount`, `order_status`, `shipping_carrier`, `payment_method`, `goods_to_declare`, `message_to_seller`, `note`, `note_update_time`, `items`, `pay_time`, `dropshipper`, `buyer_username`, `created_at`, `updated_at`) VALUES '.implode(',',$arr_order).';');
            //$implodeArray = implode("; ", $cols);
            //DB::statement($implodeArray);
        }

        $getOrderCancel = $client->order->getOrdersByStatus(['order_status' => 'CANCELLED']);
        $dataOrderCancel = $getOrderCancel->getData();
        $arr_order = array();
        $OrderSn = array();
        foreach($dataOrderCancel['orders'] as $order){
            $OrderSn[] = $order['ordersn'];
        }
        $getOrderDetails = $client->order->getOrderDetails(['ordersn_list' => $OrderSn]);
        $dataOrderDetails = $getOrderDetails->getData();
        foreach($dataOrderDetails['orders'] as $order){
            $ordersn = $order['ordersn']; //1
            $country = $order['country']; //2
            $currency = $order['currency']; //3
            $cod = $order['cod']; //4
            $tracking_no = $order['tracking_no']; //5
            $days_to_ship = $order['days_to_ship']; //6
            $recipient_address = $order['recipient_address']; //7
                $arr_address = array(
                    'town' => $recipient_address['town'],
                    'city' => $recipient_address['city'],
                    'name' => $recipient_address['name'],
                    'district' => $recipient_address['district'],
                    'country' => $recipient_address['country'],
                    'zipcode' => $recipient_address['zipcode'],
                    'full_address' => $recipient_address['full_address'],
                    'phone' => $recipient_address['phone'],
                    'state' => $recipient_address['state']
                );
                $json_address = json_encode($arr_address,JSON_UNESCAPED_UNICODE);
            $estimated_shipping_fee = $order['estimated_shipping_fee']; //8
            $actual_shipping_cost = $order['actual_shipping_cost']; //9
            $total_amount = $order['total_amount']; //10
            $escrow_amount = $order['escrow_amount']; //11
            $order_status = $order['order_status']; //12
            $shipping_carrier = $order['shipping_carrier']; //13
            $payment_method = $order['payment_method']; //14
            $goods_to_declare = $order['goods_to_declare']; //15
            $message_to_seller = $order['message_to_seller']; //16
            $note = $order['note']; //17
            $note_update_time = $order['note_update_time']; //18
            $items = $order['items']; //19
            $arr_item = NULL;
            foreach($items as $item){
                $arr_item = array(
                    'item_id' => $item['item_id'],
                    'item_name' => $item['item_name'],
                    'item_sku' => $item['item_sku'],
                    'variation_id' => $item['variation_id'],
                    'variation_name' => $item['variation_name'],
                    'variation_sku' => $item['variation_sku'],
                    'variation_quantity_purchased' => $item['variation_quantity_purchased'],
                    'variation_original_price' => $item['variation_original_price'],
                    'variation_discounted_price' => $item['variation_discounted_price'],
                    'is_wholesale' => $item['is_wholesale'],
                );
            }
            $jsonItems = json_encode($arr_item,JSON_UNESCAPED_UNICODE);
            $pay_time = $order['pay_time']; //20
            $dropshipper = $order['dropshipper']; //21
            $buyer_username = $order['buyer_username']; //22
            $create_time = date('Y-m-d H:i:s',$order['create_time']); //23
            $update_time = date('Y-m-d H:i:s',$order['update_time']); //24

            $updateData = [
                'ordersn' => $ordersn,
                'tracking_no' => $tracking_no,
                'order_status' => $order_status,
                'updated_at' => $update_time
            ];
            DB::table('orders')->where('ordersn', $ordersn)->update($updateData);

            $cols[] = 'UPDATE `orders` SET `tracking_no`="'.$tracking_no.'", `order_status`="'.$order_status.'" ,`updated_at`="'.$update_time.'" where ordersn = "'.$ordersn.'" ';

            $arr_order[] = "('".$ordersn."', '".$country."', '".$currency."', '".$cod."', '".$tracking_no."', '".$days_to_ship."', '".$json_address."', '".$estimated_shipping_fee."', '".$actual_shipping_cost."', '".$total_amount."', '".$escrow_amount."', '".$order_status."', '".$shipping_carrier."', '".$payment_method."', '".$goods_to_declare."', '".$message_to_seller."', '".$note."', '".$note_update_time."', '".$jsonItems."', '".$pay_time."', '".$dropshipper."', '".$buyer_username."', '".$create_time."', '".$update_time."')";
        }
        DB::insert('INSERT IGNORE INTO `orders` (`ordersn`, `country`, `currency`, `cod`, `tracking_no`, `days_to_ship`, `recipient_address`, `estimated_shipping_fee`, `actual_shipping_cost`, `total_amount`, `escrow_amount`, `order_status`, `shipping_carrier`, `payment_method`, `goods_to_declare`, `message_to_seller`, `note`, `note_update_time`, `items`, `pay_time`, `dropshipper`, `buyer_username`, `created_at`, `updated_at`) VALUES '.implode(',',$arr_order).';');
        
        //$implodeArray = implode("; ", $cols);
        //DB::statement($implodeArray);
        // $getOrderReturn = $client->order->getOrdersByStatus(['order_status' => 'TO_RETURN']);
        // $dataOrderReturn = $getOrderReturn->getData();
        // $arr_order = array();
        // $OrderSn = array();
        //     foreach($dataOrderReturn['orders'] as $order){
        //         $OrderSn[] = $order['ordersn'];
        //     }
        // $getOrderDetails = $client->order->getOrderDetails(['ordersn_list' => $OrderSn]);
        // $dataOrderDetails = $getOrderDetails->getData();
        // if(is_array($dataOrderDetails['orders'])){
        //     foreach($dataOrderDetails['orders'] as $order){
        //         $ordersn = $order['ordersn']; //1
        //         $country = $order['country']; //2
        //         $currency = $order['currency']; //3
        //         $cod = $order['cod']; //4
        //         $tracking_no = $order['tracking_no']; //5
        //         $days_to_ship = $order['days_to_ship']; //6
        //         $recipient_address = $order['recipient_address']; //7
        //             $arr_address = array(
        //                 'town' => $recipient_address['town'],
        //                 'city' => $recipient_address['city'],
        //                 'name' => $recipient_address['name'],
        //                 'district' => $recipient_address['district'],
        //                 'country' => $recipient_address['country'],
        //                 'zipcode' => $recipient_address['zipcode'],
        //                 'full_address' => $recipient_address['full_address'],
        //                 'phone' => $recipient_address['phone'],
        //                 'state' => $recipient_address['state']
        //             );
        //             $json_address = json_encode($arr_address,JSON_UNESCAPED_UNICODE);
        //         $estimated_shipping_fee = $order['estimated_shipping_fee']; //8
        //         $actual_shipping_cost = $order['actual_shipping_cost']; //9
        //         $total_amount = $order['total_amount']; //10
        //         $escrow_amount = $order['escrow_amount']; //11
        //         $order_status = $order['order_status']; //12
        //         $shipping_carrier = $order['shipping_carrier']; //13
        //         $payment_method = $order['payment_method']; //14
        //         $goods_to_declare = $order['goods_to_declare']; //15
        //         $message_to_seller = $order['message_to_seller']; //16
        //         $note = $order['note']; //17
        //         $note_update_time = $order['note_update_time']; //18
        //         $items = $order['items']; //19
        //         $arr_item = NULL;
        //         foreach($items as $item){
        //             $arr_item = array(
        //                 'item_id' => $item['item_id'],
        //                 'item_name' => $item['item_name'],
        //                 'item_sku' => $item['item_sku'],
        //                 'variation_id' => $item['variation_id'],
        //                 'variation_name' => $item['variation_name'],
        //                 'variation_sku' => $item['variation_sku'],
        //                 'variation_quantity_purchased' => $item['variation_quantity_purchased'],
        //                 'variation_original_price' => $item['variation_original_price'],
        //                 'variation_discounted_price' => $item['variation_discounted_price'],
        //                 'is_wholesale' => $item['is_wholesale'],
        //             );
        //         }
        //         $jsonItems = json_encode($arr_item,JSON_UNESCAPED_UNICODE);
        //         $pay_time = $order['pay_time']; //20
        //         $dropshipper = $order['dropshipper']; //21
        //         $buyer_username = $order['buyer_username']; //22
        //         $create_time = date('Y-m-d H:i:s',$order['create_time']); //23
        //         $update_time = date('Y-m-d H:i:s',$order['update_time']); //24

        //         $arr_order[] = "('".$ordersn."', '".$country."', '".$currency."', '".$cod."', '".$tracking_no."', '".$days_to_ship."', '".$json_address."', '".$estimated_shipping_fee."', '".$actual_shipping_cost."', '".$total_amount."', '".$escrow_amount."', '".$order_status."', '".$shipping_carrier."', '".$payment_method."', '".$goods_to_declare."', '".$message_to_seller."', '".$note."', '".$note_update_time."', '".$jsonItems."', '".$pay_time."', '".$dropshipper."', '".$buyer_username."', '".$create_time."', '".$update_time."')";
        //     }
        // }
        // DB::insert('INSERT IGNORE INTO `orders` (`ordersn`, `country`, `currency`, `cod`, `tracking_no`, `days_to_ship`, `recipient_address`, `estimated_shipping_fee`, `actual_shipping_cost`, `total_amount`, `escrow_amount`, `order_status`, `shipping_carrier`, `payment_method`, `goods_to_declare`, `message_to_seller`, `note`, `note_update_time`, `items`, `pay_time`, `dropshipper`, `buyer_username`, `created_at`, `updated_at`) VALUES '.implode(',',$arr_order).';');
        //--------------------------OrderEscrow-----------------------------------------------//
        // $getOrderUNPAID = $client->order->getOrdersByStatus(['order_status' => 'UNPAID']);
        // $dataOrderUNPAID = $getOrderUNPAID->getData();
        // $arr_order = array();
        // $arr_orderStatus = NULL;
        // $arr_timeOrder = NULL;
        // foreach($dataOrderUNPAID['orders'] as $order){
        //     $arr_orderStatus = $order['order_status'];
        //     $arr_timeOrder = date("Y-m-d H:i:s",$order['update_time']);
        //     $OrderSn = $order['ordersn'].",";
        //     $ex_SnID = explode(',', $OrderSn);
        //     $getOrderDetails = $client->order->getEscrowDetails(['ordersn' => (string)$ex_SnID[0]]);
        //     $dataOrderDetails = $getOrderDetails->getData();
        //     $orderSn = $dataOrderDetails['order']['ordersn'];
        //     $activity = $dataOrderDetails['order']['activity'];
        //         $arr_act = NULL;
        //     foreach ($activity as $act) {
        //         if(is_array($arr_act)){
        //             $arr_act = array(
        //                 'activity_id' => $act['activity_id'],
        //                 'activity_type' => $act['activity_type'],
        //                 'original_price' => $act['original_price'],
        //                 'discounted_price' => $act['discounted_price']
        //             );
        //         }
        //     }
        //     $json_Act = json_encode($arr_act,JSON_UNESCAPED_UNICODE);

        //     $payee_id = $dataOrderDetails['order']['payee_id'];
        //     $shipping_carrier = $dataOrderDetails['order']['shipping_carrier'];
        //     $exchange_rate = $dataOrderDetails['order']['exchange_rate'];
        //     $bank_account = $dataOrderDetails['order']['bank_account'];
        //     if(!empty($bank_account)){
        //         $arr_bank = array(
        //             'bank_name' => $bank_account['bank_name'],
        //             'bank_account_number' => $bank_account['bank_account_number'],
        //             'bank_account_country' => $bank_account['bank_account_country']
        //         );
        //     }
        //     $json_bank = json_encode($arr_bank,JSON_UNESCAPED_UNICODE);

        //     $income_details = $dataOrderDetails['order']['income_details'];
        //     $arr_income_details = array(
        //         'local_currency' => $income_details['local_currency'],
        //         'total_amount' => $income_details['total_amount'],
        //         'coin' => $income_details['coin'],
        //         'voucher' => $income_details['voucher'],
        //         'voucher_seller' => $income_details['voucher_seller'],
        //         'seller_rebate' => $income_details['seller_rebate'],
        //         'actual_shipping_cost' => $income_details['actual_shipping_cost'],
        //         'shipping_fee_rebate' => $income_details['shipping_fee_rebate'],
        //         'commission_fee' => $income_details['commission_fee'],
        //         'voucher_code' => $income_details['voucher_code'],
        //         'voucher_name' => $income_details['voucher_name'],
        //         'escrow_amount' => $income_details['escrow_amount'],
        //         'cross_border_tax' => $income_details['cross_border_tax'],
        //         'credit_card_promotion' => $income_details['credit_card_promotion'],
        //         'credit_card_transaction_fee' => $income_details['credit_card_transaction_fee']
        //     );
        //     $json_incomeDetails = json_encode($arr_income_details,JSON_UNESCAPED_UNICODE);

        //     $country = $dataOrderDetails['order']['country'];
        //     $escrow_currency = $dataOrderDetails['order']['escrow_currency'];
        //     $escrow_channel = $dataOrderDetails['order']['escrow_channel'];
        //     $items = $dataOrderDetails['order']['items'];
        //         $arr_OrderItems = NULL;
        //     foreach ($items as $item) {
        //         if(is_array($items)){
        //             $arr_OrderItems = array(
        //                 'item_id' => $item['item_id'],
        //                 'item_name' => $item['item_name'],
        //                 'item_sku' => $item['item_sku'],
        //                 'variation_id' => $item['variation_id'],
        //                 'variation_name' => $item['variation_name'],
        //                 'variation_sku' => $item['variation_sku'],
        //                 'quantity_purchased' => $item['quantity_purchased'],
        //                 'original_price' => $item['original_price'],
        //                 'discount_from_coin' => $item['discount_from_coin'],
        //                 'discount_from_voucher' => $item['discount_from_voucher'],
        //                 'discount_from_voucher_seller' => $item['discount_from_voucher_seller'],
        //                 'seller_rebate' => $item['seller_rebate'],
        //                 'deal_price' => $item['deal_price'],
        //                 'credit_card_promotion' => $item['credit_card_promotion'],
        //             );
        //         }
        //     }
        //     $json_OrderItem = json_encode($arr_OrderItems,JSON_UNESCAPED_UNICODE);

        //     $arr_order[] = "('".$orderSn."', '".$json_Act."', '".$payee_id."', '".$shipping_carrier."', '".$exchange_rate."', '".$json_bank."', '".$json_incomeDetails."', '".$country."', '".$escrow_currency."', '".$escrow_channel."', '".$json_OrderItem."', '".$arr_orderStatus."', '".$arr_timeOrder."')";
        // }
        // DB::insert('INSERT IGNORE INTO order_escrow (ordersn, activity, payee_id, shipping_carrier, exchange_rate, bank_account, income_details, country, escrow_currency, escrow_channel, items, status, created_at) VALUES '.implode(',',$arr_order).';');

        // $getOrderReady = $client->order->getOrdersByStatus(['order_status' => 'READY_TO_SHIP']);
        // $dataOrderReady = $getOrderReady->getData();
        // $arr_order = array();
        // $arr_orderStatus = NULL;
        // $arr_timeOrder = NULL;
        // foreach($dataOrderReady['orders'] as $order){
        //     $arr_orderStatus = $order['order_status'];
        //     $arr_timeOrder = date("Y-m-d H:i:s",$order['update_time']);
        //     $OrderSn = $order['ordersn'].",";
        //     $ex_SnID = explode(',', $OrderSn);
        //     $getOrderDetails = $client->order->getEscrowDetails(['ordersn' => (string)$ex_SnID[0]]);
        //     $dataOrderDetails = $getOrderDetails->getData();
        //     $orderSn = $dataOrderDetails['order']['ordersn'];
        //     $activity = $dataOrderDetails['order']['activity'];
        //         $arr_act = NULL;
        //     foreach ($activity as $act) {
        //         if(is_array($arr_act)){
        //             $arr_act = array(
        //                 'activity_id' => $act['activity_id'],
        //                 'activity_type' => $act['activity_type'],
        //                 'original_price' => $act['original_price'],
        //                 'discounted_price' => $act['discounted_price']
        //             );
        //         }
        //     }
        //     $json_Act = json_encode($arr_act,JSON_UNESCAPED_UNICODE);

        //     $payee_id = $dataOrderDetails['order']['payee_id'];
        //     $shipping_carrier = $dataOrderDetails['order']['shipping_carrier'];
        //     $exchange_rate = $dataOrderDetails['order']['exchange_rate'];
        //     $bank_account = $dataOrderDetails['order']['bank_account'];
        //     if(!empty($bank_account)){
        //         $arr_bank = array(
        //             'bank_name' => $bank_account['bank_name'],
        //             'bank_account_number' => $bank_account['bank_account_number'],
        //             'bank_account_country' => $bank_account['bank_account_country']
        //         );
        //     }
        //     $json_bank = json_encode($arr_bank,JSON_UNESCAPED_UNICODE);

        //     $income_details = $dataOrderDetails['order']['income_details'];
        //     $arr_income_details = array(
        //         'local_currency' => $income_details['local_currency'],
        //         'total_amount' => $income_details['total_amount'],
        //         'coin' => $income_details['coin'],
        //         'voucher' => $income_details['voucher'],
        //         'voucher_seller' => $income_details['voucher_seller'],
        //         'seller_rebate' => $income_details['seller_rebate'],
        //         'actual_shipping_cost' => $income_details['actual_shipping_cost'],
        //         'shipping_fee_rebate' => $income_details['shipping_fee_rebate'],
        //         'commission_fee' => $income_details['commission_fee'],
        //         'voucher_code' => $income_details['voucher_code'],
        //         'voucher_name' => $income_details['voucher_name'],
        //         'escrow_amount' => $income_details['escrow_amount'],
        //         'cross_border_tax' => $income_details['cross_border_tax'],
        //         'credit_card_promotion' => $income_details['credit_card_promotion'],
        //         'credit_card_transaction_fee' => $income_details['credit_card_transaction_fee']
        //     );
        //     $json_incomeDetails = json_encode($arr_income_details,JSON_UNESCAPED_UNICODE);

        //     $country = $dataOrderDetails['order']['country'];
        //     $escrow_currency = $dataOrderDetails['order']['escrow_currency'];
        //     $escrow_channel = $dataOrderDetails['order']['escrow_channel'];
        //     $items = $dataOrderDetails['order']['items'];
        //         $arr_OrderItems = NULL;
        //     foreach ($items as $item) {
        //         if(is_array($items)){
        //             $arr_OrderItems = array(
        //                 'item_id' => $item['item_id'],
        //                 'item_name' => $item['item_name'],
        //                 'item_sku' => $item['item_sku'],
        //                 'variation_id' => $item['variation_id'],
        //                 'variation_name' => $item['variation_name'],
        //                 'variation_sku' => $item['variation_sku'],
        //                 'quantity_purchased' => $item['quantity_purchased'],
        //                 'original_price' => $item['original_price'],
        //                 'discount_from_coin' => $item['discount_from_coin'],
        //                 'discount_from_voucher' => $item['discount_from_voucher'],
        //                 'discount_from_voucher_seller' => $item['discount_from_voucher_seller'],
        //                 'seller_rebate' => $item['seller_rebate'],
        //                 'deal_price' => $item['deal_price'],
        //                 'credit_card_promotion' => $item['credit_card_promotion'],
        //             );
        //         }
        //     }
        //     $json_OrderItem = json_encode($arr_OrderItems,JSON_UNESCAPED_UNICODE);

        //     $arr_order[] = "('".$orderSn."', '".$json_Act."', '".$payee_id."', '".$shipping_carrier."', '".$exchange_rate."', '".$json_bank."', '".$json_incomeDetails."', '".$country."', '".$escrow_currency."', '".$escrow_channel."', '".$arr_orderStatus."', '".$json_OrderItem."', '".$arr_timeOrder."')";
        // }
        // DB::insert('INSERT IGNORE INTO order_escrow (ordersn, activity, payee_id, shipping_carrier, exchange_rate, bank_account, income_details, country, escrow_currency, escrow_channel, status, items, created_at) VALUES '.implode(',',$arr_order).';');

        // $getOrderComplete = $client->order->getOrdersByStatus(['order_status' => 'COMPLETED']);
        // $dataOrderComplete = $getOrderComplete->getData();
        // $arr_order = array();
        // $arr_orderStatus = NULL;
        // $arr_timeOrder = NULL;
        // foreach($dataOrderComplete['orders'] as $order){
        //     $arr_orderStatus = $order['order_status'];
        //     $arr_timeOrder = date("Y-m-d H:i:s",$order['update_time']);
        //     $OrderSn = $order['ordersn'].",";
        //     $ex_SnID = explode(',', $OrderSn);
        //     $getOrderDetails = $client->order->getEscrowDetails(['ordersn' => (string)$ex_SnID[0]]);
        //     $dataOrderDetails = $getOrderDetails->getData();
        //     $orderSn = $dataOrderDetails['order']['ordersn'];
        //     $activity = $dataOrderDetails['order']['activity'];
        //         $arr_act = NULL;
        //     foreach ($activity as $act) {
        //         if(is_array($arr_act)){
        //             $arr_act = array(
        //                 'activity_id' => $act['activity_id'],
        //                 'activity_type' => $act['activity_type'],
        //                 'original_price' => $act['original_price'],
        //                 'discounted_price' => $act['discounted_price']
        //             );
        //         }
        //     }
        //     $json_Act = json_encode($arr_act,JSON_UNESCAPED_UNICODE);

        //     $payee_id = $dataOrderDetails['order']['payee_id'];
        //     $shipping_carrier = $dataOrderDetails['order']['shipping_carrier'];
        //     $exchange_rate = $dataOrderDetails['order']['exchange_rate'];
        //     $bank_account = $dataOrderDetails['order']['bank_account'];
        //     if(!empty($bank_account)){
        //         $arr_bank = array(
        //             'bank_name' => $bank_account['bank_name'],
        //             'bank_account_number' => $bank_account['bank_account_number'],
        //             'bank_account_country' => $bank_account['bank_account_country']
        //         );
        //     }
        //     $json_bank = json_encode($arr_bank,JSON_UNESCAPED_UNICODE);

        //     $income_details = $dataOrderDetails['order']['income_details'];
        //     $arr_income_details = array(
        //         'local_currency' => $income_details['local_currency'],
        //         'total_amount' => $income_details['total_amount'],
        //         'coin' => $income_details['coin'],
        //         'voucher' => $income_details['voucher'],
        //         'voucher_seller' => $income_details['voucher_seller'],
        //         'seller_rebate' => $income_details['seller_rebate'],
        //         'actual_shipping_cost' => $income_details['actual_shipping_cost'],
        //         'shipping_fee_rebate' => $income_details['shipping_fee_rebate'],
        //         'commission_fee' => $income_details['commission_fee'],
        //         'voucher_code' => $income_details['voucher_code'],
        //         'voucher_name' => $income_details['voucher_name'],
        //         'escrow_amount' => $income_details['escrow_amount'],
        //         'cross_border_tax' => $income_details['cross_border_tax'],
        //         'credit_card_promotion' => $income_details['credit_card_promotion'],
        //         'credit_card_transaction_fee' => $income_details['credit_card_transaction_fee']
        //     );
        //     $json_incomeDetails = json_encode($arr_income_details,JSON_UNESCAPED_UNICODE);

        //     $country = $dataOrderDetails['order']['country'];
        //     $escrow_currency = $dataOrderDetails['order']['escrow_currency'];
        //     $escrow_channel = $dataOrderDetails['order']['escrow_channel'];
        //     $items = $dataOrderDetails['order']['items'];
        //         $arr_OrderItems = NULL;
        //     foreach ($items as $item) {
        //         if(is_array($items)){
        //             $arr_OrderItems = array(
        //                 'item_id' => $item['item_id'],
        //                 'item_name' => $item['item_name'],
        //                 'item_sku' => $item['item_sku'],
        //                 'variation_id' => $item['variation_id'],
        //                 'variation_name' => $item['variation_name'],
        //                 'variation_sku' => $item['variation_sku'],
        //                 'quantity_purchased' => $item['quantity_purchased'],
        //                 'original_price' => $item['original_price'],
        //                 'discount_from_coin' => $item['discount_from_coin'],
        //                 'discount_from_voucher' => $item['discount_from_voucher'],
        //                 'discount_from_voucher_seller' => $item['discount_from_voucher_seller'],
        //                 'seller_rebate' => $item['seller_rebate'],
        //                 'deal_price' => $item['deal_price'],
        //                 'credit_card_promotion' => $item['credit_card_promotion'],
        //             );
        //         }
        //     }
        //     $json_OrderItem = json_encode($arr_OrderItems,JSON_UNESCAPED_UNICODE);

        //     $arr_order[] = "('".$orderSn."', '".$json_Act."', '".$payee_id."', '".$shipping_carrier."', '".$exchange_rate."', '".$json_bank."', '".$json_incomeDetails."', '".$country."', '".$escrow_currency."', '".$escrow_channel."', '".$arr_orderStatus."', '".$json_OrderItem."', '".$arr_timeOrder."')";
        // }
        // DB::insert('INSERT IGNORE INTO order_escrow (ordersn, activity, payee_id, shipping_carrier, exchange_rate, bank_account, income_details, country, escrow_currency, escrow_channel, status, items, created_at) VALUES '.implode(',',$arr_order).';');

        // $getOrderCANCEL = $client->order->getOrdersByStatus(['order_status' => 'IN_CANCEL']);
        // $dataOrderCANCEL = $getOrderCANCEL->getData();
        // if(is_array($dataOrderCANCEL['orders'])){
        //     $arr_order = array();
        //     $arr_orderStatus = NULL;
        //     $arr_timeOrder = NULL;
        //     foreach($dataOrderCANCEL['orders'] as $order){
        //         $arr_orderStatus = $order['order_status'];
        //         $arr_timeOrder = date("Y-m-d H:i:s", $order['update_time']);
        //         $OrderSn = $order['ordersn'].",";
        //         $ex_SnID = explode(',', $OrderSn);
        //         $getOrderDetails = $client->order->getEscrowDetails(['ordersn' => (string)$ex_SnID[0]]);
        //         $dataOrderDetails = $getOrderDetails->getData();
        //         $orderSn = $dataOrderDetails['order']['ordersn'];
        //         $activity = $dataOrderDetails['order']['activity'];
        //             $arr_act = NULL;
        //         foreach ($activity as $act) {
        //             if(is_array($arr_act)){
        //                 $arr_act = array(
        //                     'activity_id' => $act['activity_id'],
        //                     'activity_type' => $act['activity_type'],
        //                     'original_price' => $act['original_price'],
        //                     'discounted_price' => $act['discounted_price']
        //                 );
        //             }
        //         }
        //         $json_Act = json_encode($arr_act,JSON_UNESCAPED_UNICODE);

        //         $payee_id = $dataOrderDetails['order']['payee_id'];
        //         $shipping_carrier = $dataOrderDetails['order']['shipping_carrier'];
        //         $exchange_rate = $dataOrderDetails['order']['exchange_rate'];
        //         $bank_account = $dataOrderDetails['order']['bank_account'];
        //         if(!empty($bank_account)){
        //             $arr_bank = array(
        //                 'bank_name' => $bank_account['bank_name'],
        //                 'bank_account_number' => $bank_account['bank_account_number'],
        //                 'bank_account_country' => $bank_account['bank_account_country']
        //             );
        //         }
        //         $json_bank = json_encode($arr_bank,JSON_UNESCAPED_UNICODE);

        //         $income_details = $dataOrderDetails['order']['income_details'];
        //         $arr_income_details = array(
        //             'local_currency' => $income_details['local_currency'],
        //             'total_amount' => $income_details['total_amount'],
        //             'coin' => $income_details['coin'],
        //             'voucher' => $income_details['voucher'],
        //             'voucher_seller' => $income_details['voucher_seller'],
        //             'seller_rebate' => $income_details['seller_rebate'],
        //             'actual_shipping_cost' => $income_details['actual_shipping_cost'],
        //             'shipping_fee_rebate' => $income_details['shipping_fee_rebate'],
        //             'commission_fee' => $income_details['commission_fee'],
        //             'voucher_code' => $income_details['voucher_code'],
        //             'voucher_name' => $income_details['voucher_name'],
        //             'escrow_amount' => $income_details['escrow_amount'],
        //             'cross_border_tax' => $income_details['cross_border_tax'],
        //             'credit_card_promotion' => $income_details['credit_card_promotion'],
        //             'credit_card_transaction_fee' => $income_details['credit_card_transaction_fee']
        //         );
        //         $json_incomeDetails = json_encode($arr_income_details,JSON_UNESCAPED_UNICODE);

        //         $country = $dataOrderDetails['order']['country'];
        //         $escrow_currency = $dataOrderDetails['order']['escrow_currency'];
        //         $escrow_channel = $dataOrderDetails['order']['escrow_channel'];
        //         $items = $dataOrderDetails['order']['items'];
        //             $arr_OrderItems = NULL;
        //         foreach ($items as $item) {
        //             if(is_array($items)){
        //                 $arr_OrderItems = array(
        //                     'item_id' => $item['item_id'],
        //                     'item_name' => $item['item_name'],
        //                     'item_sku' => $item['item_sku'],
        //                     'variation_id' => $item['variation_id'],
        //                     'variation_name' => $item['variation_name'],
        //                     'variation_sku' => $item['variation_sku'],
        //                     'quantity_purchased' => $item['quantity_purchased'],
        //                     'original_price' => $item['original_price'],
        //                     'discount_from_coin' => $item['discount_from_coin'],
        //                     'discount_from_voucher' => $item['discount_from_voucher'],
        //                     'discount_from_voucher_seller' => $item['discount_from_voucher_seller'],
        //                     'seller_rebate' => $item['seller_rebate'],
        //                     'deal_price' => $item['deal_price'],
        //                     'credit_card_promotion' => $item['credit_card_promotion'],
        //                 );
        //             }
        //         }
        //         $json_OrderItem = json_encode($arr_OrderItems,JSON_UNESCAPED_UNICODE);

        //         $arr_order[] = "('".$orderSn."', '".$json_Act."', '".$payee_id."', '".$shipping_carrier."', '".$exchange_rate."', '".$json_bank."', '".$json_incomeDetails."', '".$country."', '".$escrow_currency."', '".$escrow_channel."', '".$arr_orderStatus."', '".$json_OrderItem."', '".$arr_timeOrder."')";
        //     }
        //     DB::insert('INSERT IGNORE INTO order_escrow (ordersn, activity, payee_id, shipping_carrier, exchange_rate, bank_account, income_details, country, escrow_currency, escrow_channel, status, items, created_at) VALUES '.implode(',',$arr_order).';');
        // }
        // $getOrderCANCELLED = $client->order->getOrdersByStatus(['order_status' => 'CANCELLED']);
        // $dataOrderCANCELLED = $getOrderCANCELLED->getData();
        // $arr_order = array();
        // $arr_orderStatus = NULL;
        // $arr_timeOrder = NULL;
        // foreach($dataOrderCANCELLED['orders'] as $order){
        //     $arr_orderStatus = $order['order_status'];
        //     $arr_timeOrder = date("Y-m-d H:i:s", $order['update_time']);
        //     $OrderSn = $order['ordersn'].",";
        //     $ex_SnID = explode(',', $OrderSn);
        //     $getOrderDetails = $client->order->getEscrowDetails(['ordersn' => (string)$ex_SnID[0]]);
        //     $dataOrderDetails = $getOrderDetails->getData();
        //     $orderSn = $dataOrderDetails['order']['ordersn'];
        //     $activity = $dataOrderDetails['order']['activity'];
        //         $arr_act = NULL;
        //     foreach ($activity as $act) {
        //         if(is_array($arr_act)){
        //             $arr_act = array(
        //                 'activity_id' => $act['activity_id'],
        //                 'activity_type' => $act['activity_type'],
        //                 'original_price' => $act['original_price'],
        //                 'discounted_price' => $act['discounted_price']
        //             );
        //         }
        //     }
        //     $json_Act = json_encode($arr_act,JSON_UNESCAPED_UNICODE);

        //     $payee_id = $dataOrderDetails['order']['payee_id'];
        //     $shipping_carrier = $dataOrderDetails['order']['shipping_carrier'];
        //     $exchange_rate = $dataOrderDetails['order']['exchange_rate'];
        //     $bank_account = $dataOrderDetails['order']['bank_account'];
        //     if(!empty($bank_account)){
        //         $arr_bank = array(
        //             'bank_name' => $bank_account['bank_name'],
        //             'bank_account_number' => $bank_account['bank_account_number'],
        //             'bank_account_country' => $bank_account['bank_account_country']
        //         );
        //     }
        //     $json_bank = json_encode($arr_bank,JSON_UNESCAPED_UNICODE);

        //     $income_details = $dataOrderDetails['order']['income_details'];
        //     $arr_income_details = array(
        //         'local_currency' => $income_details['local_currency'],
        //         'total_amount' => $income_details['total_amount'],
        //         'coin' => $income_details['coin'],
        //         'voucher' => $income_details['voucher'],
        //         'voucher_seller' => $income_details['voucher_seller'],
        //         'seller_rebate' => $income_details['seller_rebate'],
        //         'actual_shipping_cost' => $income_details['actual_shipping_cost'],
        //         'shipping_fee_rebate' => $income_details['shipping_fee_rebate'],
        //         'commission_fee' => $income_details['commission_fee'],
        //         'voucher_code' => $income_details['voucher_code'],
        //         'voucher_name' => $income_details['voucher_name'],
        //         'escrow_amount' => $income_details['escrow_amount'],
        //         'cross_border_tax' => $income_details['cross_border_tax'],
        //         'credit_card_promotion' => $income_details['credit_card_promotion'],
        //         'credit_card_transaction_fee' => $income_details['credit_card_transaction_fee']
        //     );
        //     $json_incomeDetails = json_encode($arr_income_details,JSON_UNESCAPED_UNICODE);

        //     $country = $dataOrderDetails['order']['country'];
        //     $escrow_currency = $dataOrderDetails['order']['escrow_currency'];
        //     $escrow_channel = $dataOrderDetails['order']['escrow_channel'];
        //     $items = $dataOrderDetails['order']['items'];
        //         $arr_OrderItems = NULL;
        //     foreach ($items as $item) {
        //         if(is_array($items)){
        //             $arr_OrderItems = array(
        //                 'item_id' => $item['item_id'],
        //                 'item_name' => $item['item_name'],
        //                 'item_sku' => $item['item_sku'],
        //                 'variation_id' => $item['variation_id'],
        //                 'variation_name' => $item['variation_name'],
        //                 'variation_sku' => $item['variation_sku'],
        //                 'quantity_purchased' => $item['quantity_purchased'],
        //                 'original_price' => $item['original_price'],
        //                 'discount_from_coin' => $item['discount_from_coin'],
        //                 'discount_from_voucher' => $item['discount_from_voucher'],
        //                 'discount_from_voucher_seller' => $item['discount_from_voucher_seller'],
        //                 'seller_rebate' => $item['seller_rebate'],
        //                 'deal_price' => $item['deal_price'],
        //                 'credit_card_promotion' => $item['credit_card_promotion'],
        //             );
        //         }
        //     }
        //     $json_OrderItem = json_encode($arr_OrderItems,JSON_UNESCAPED_UNICODE);

        //     $arr_order[] = "('".$orderSn."', '".$json_Act."', '".$payee_id."', '".$shipping_carrier."', '".$exchange_rate."', '".$json_bank."', '".$json_incomeDetails."', '".$country."', '".$escrow_currency."', '".$escrow_channel."', '".$arr_orderStatus."', '".$json_OrderItem."', '".$arr_timeOrder."')";
        // }
        // DB::insert('INSERT IGNORE INTO order_escrow (ordersn, activity, payee_id, shipping_carrier, exchange_rate, bank_account, income_details, country, escrow_currency, escrow_channel, status, items, created_at) VALUES '.implode(',',$arr_order).';');


        // $getOrderReturn = $client->order->getOrdersByStatus(['order_status' => 'TO_RETURN']);
        // $dataOrderReturn = $getOrderReturn->getData();
        // $arr_order = array();
        // $arr_orderStatus = NULL;
        // $arr_timeOrder = NULL;
        // foreach($dataOrderReturn['orders'] as $order){
        //     $arr_orderStatus = $order['order_status'];
        //     $arr_timeOrder = date("Y-m-d H:i:s", $order['update_time']);
        //     $OrderSn = $order['ordersn'].",";
        //     $ex_SnID = explode(',', $OrderSn);
        //     $getOrderDetails = $client->order->getEscrowDetails(['ordersn' => (string)$ex_SnID[0]]);
        //     $dataOrderDetails = $getOrderDetails->getData();
        //     $orderSn = $dataOrderDetails['order']['ordersn'];
        //     $activity = $dataOrderDetails['order']['activity'];
        //         $arr_act = NULL;
        //     foreach ($activity as $act) {
        //         if(is_array($arr_act)){
        //             $arr_act = array(
        //                 'activity_id' => $act['activity_id'],
        //                 'activity_type' => $act['activity_type'],
        //                 'original_price' => $act['original_price'],
        //                 'discounted_price' => $act['discounted_price']
        //             );
        //         }
        //     }
        //     $json_Act = json_encode($arr_act,JSON_UNESCAPED_UNICODE);

        //     $payee_id = $dataOrderDetails['order']['payee_id'];
        //     $shipping_carrier = $dataOrderDetails['order']['shipping_carrier'];
        //     $exchange_rate = $dataOrderDetails['order']['exchange_rate'];
        //     $bank_account = $dataOrderDetails['order']['bank_account'];
        //     if(!empty($bank_account)){
        //         $arr_bank = array(
        //             'bank_name' => $bank_account['bank_name'],
        //             'bank_account_number' => $bank_account['bank_account_number'],
        //             'bank_account_country' => $bank_account['bank_account_country']
        //         );
        //     }
        //     $json_bank = json_encode($arr_bank,JSON_UNESCAPED_UNICODE);

        //     $income_details = $dataOrderDetails['order']['income_details'];
        //     $arr_income_details = array(
        //         'local_currency' => $income_details['local_currency'],
        //         'total_amount' => $income_details['total_amount'],
        //         'coin' => $income_details['coin'],
        //         'voucher' => $income_details['voucher'],
        //         'voucher_seller' => $income_details['voucher_seller'],
        //         'seller_rebate' => $income_details['seller_rebate'],
        //         'actual_shipping_cost' => $income_details['actual_shipping_cost'],
        //         'shipping_fee_rebate' => $income_details['shipping_fee_rebate'],
        //         'commission_fee' => $income_details['commission_fee'],
        //         'voucher_code' => $income_details['voucher_code'],
        //         'voucher_name' => $income_details['voucher_name'],
        //         'escrow_amount' => $income_details['escrow_amount'],
        //         'cross_border_tax' => $income_details['cross_border_tax'],
        //         'credit_card_promotion' => $income_details['credit_card_promotion'],
        //         'credit_card_transaction_fee' => $income_details['credit_card_transaction_fee']
        //     );
        //     $json_incomeDetails = json_encode($arr_income_details,JSON_UNESCAPED_UNICODE);

        //     $country = $dataOrderDetails['order']['country'];
        //     $escrow_currency = $dataOrderDetails['order']['escrow_currency'];
        //     $escrow_channel = $dataOrderDetails['order']['escrow_channel'];
        //     $items = $dataOrderDetails['order']['items'];
        //         $arr_OrderItems = NULL;
        //     foreach ($items as $item) {
        //         if(is_array($items)){
        //             $arr_OrderItems = array(
        //                 'item_id' => $item['item_id'],
        //                 'item_name' => $item['item_name'],
        //                 'item_sku' => $item['item_sku'],
        //                 'variation_id' => $item['variation_id'],
        //                 'variation_name' => $item['variation_name'],
        //                 'variation_sku' => $item['variation_sku'],
        //                 'quantity_purchased' => $item['quantity_purchased'],
        //                 'original_price' => $item['original_price'],
        //                 'discount_from_coin' => $item['discount_from_coin'],
        //                 'discount_from_voucher' => $item['discount_from_voucher'],
        //                 'discount_from_voucher_seller' => $item['discount_from_voucher_seller'],
        //                 'seller_rebate' => $item['seller_rebate'],
        //                 'deal_price' => $item['deal_price'],
        //                 'credit_card_promotion' => $item['credit_card_promotion'],
        //             );
        //         }
        //     }
        //     $json_OrderItem = json_encode($arr_OrderItems,JSON_UNESCAPED_UNICODE);

        //     $arr_order[] = "('".$orderSn."', '".$json_Act."', '".$payee_id."', '".$shipping_carrier."', '".$exchange_rate."', '".$json_bank."', '".$json_incomeDetails."', '".$country."', '".$escrow_currency."', '".$escrow_channel."', '".$arr_orderStatus."', '".$json_OrderItem."', '".$arr_timeOrder."')";
        // }
        // DB::insert('INSERT IGNORE INTO order_escrow (ordersn, activity, payee_id, shipping_carrier, exchange_rate, bank_account, income_details, country, escrow_currency, escrow_channel, status, items, created_at) VALUES '.implode(',',$arr_order).';');

        return response()->json(['status' => true]);
    }

}
