<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Payment;
use App\Product;
use App\Shop;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use PDF;
use DB;
use Picqer\Barcode;
use BaconQrCode;
use \Milon\Barcode\DNS1D;


class OrderController extends Controller
{
// For demonstration purposes, get pararameters that are passed in through $_GET or set to the default value
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //Pagination//
        $pagi = $request->get('pagination') ?? 100;
        $orders = Order::orderBy('created_at', 'DESC')->paginate($pagi);
        //SearchData//
        if(!empty($request->get('ordersn'))){
            $ordersn = $request->ordersn ? $request->ordersn : 0;
            $orders = Order::where('ordersn', 'LIKE', '%'.$ordersn.'%')->paginate($pagi);
        }
        if(!empty($request->get('name_customer'))){
            $request->session()->put('name_customer', $request->has('name_customer') ? $request->get('name_customer') : ($request->session()->has('ordersn') ? $request->session()->get('name_customer') : ''));
            $orders = Order::where('recipient_address', 'LIKE', '%"name":"%'.$request->session()->get('name_customer').'%"%')->paginate(10);
        }
        if(!empty($request->get('product'))){
            $orders = Order::where('items', 'LIKE', '%"item_name":"%'.$request->get('product').'%"%')->paginate($pagi);
        }
        if(!empty($request->get('item_sku'))){
            $orders = Order::where('items', 'LIKE', '%"item_sku":"%'.$request->get('item_sku').'%"%')->paginate($pagi);
        }
        if(!empty($request->get('payments'))){
            $orders = Order::where('payment_method', 'LIKE', '%'.$request->get('payments').'%')->paginate($pagi);
        }
        if(!empty($request->get('date'))){
            $subStr_Date = explode(' - ',$request->get('date'));
            $startDate = new Carbon($subStr_Date[0]);
            $endDate = new Carbon($subStr_Date[1]);
            $orders = Order::whereBetween('created_at', array($startDate->toDateString(),$endDate->toDateString()))->paginate($pagi);
        }
        //FiterData//
        $sortby = $request->get('sortby') ? $request->get('sortby') : '';
        $order = $request->get('order') ? $request->get('order') : 'DESC';
        if($sortby == 'ordersn'){
            $orders = Order::orderBy('ordersn', $order)->paginate($pagi);
        }
        if($sortby == 'date'){
            $orders = Order::orderBy('created_at', $order)->paginate($pagi);
        }
        if($sortby == 'item_sku'){
            $orders = Order::orderBy('items', $order)
                    ->paginate($pagi);
        }
        $payments = Payment::get();

        if($request->ajax()){
            return view('order.index', compact('orders', 'payments'))
                ->withPagination($pagi);
        }else{
            return view('order.index', compact('orders', 'payments'))
                ->withPagination($pagi);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataUnpaid(Request $request)
    {
        $pagi = $request->get('pagination') ?? 100;
        $orders = Order::where('order_status', '=', 'UNPAID')->orderBy('created_at', 'DESC')->paginate($pagi);
        $payments = Payment::get();
        //SearchData//
        if(!empty($request->get('ordersn'))){
            $ordersn = $request->ordersn ? $request->ordersn : 0;
            $orders = Order::where('ordersn', 'LIKE', '%'.$ordersn.'%')->paginate($pagi);
        }
        if(!empty($request->get('name_customer'))){
            $request->session()->put('name_customer', $request->has('name_customer') ? $request->get('name_customer') : ($request->session()->has('ordersn') ? $request->session()->get('name_customer') : ''));
            $orders = Order::where('recipient_address', 'LIKE', '%"name":"%'.$request->session()->get('name_customer').'%"%')->paginate(10);
        }
        if(!empty($request->get('product'))){
            $orders = Order::where('items', 'LIKE', '%"item_name":"%'.$request->get('product').'%"%')->paginate($pagi);
        }
        if(!empty($request->get('item_sku'))){
            $orders = Order::where('items', 'LIKE', '%"item_sku":"%'.$request->get('item_sku').'%"%')->paginate($pagi);
        }
        if(!empty($request->get('payments'))){
            $orders = Order::where('payment_method', 'LIKE', '%'.$request->get('payments').'%')->paginate($pagi);
        }
        if(!empty($request->get('date'))){
            $subStr_Date = explode(' - ',$request->get('date'));
            $startDate = new Carbon($subStr_Date[0]);
            $endDate = new Carbon($subStr_Date[1]);
            $orders = Order::whereBetween('created_at', array($startDate->toDateString(),$endDate->toDateString()))->paginate($pagi);
        }
        //FiterData//
        $sortby = $request->get('sortby') ? $request->get('sortby') : '';
        $order = $request->get('order') ? $request->get('order') : 'DESC';
        if($sortby == 'ordersn'){
            $orders = Order::orderBy('ordersn', $order)->paginate($pagi);
        }
        if($sortby == 'date'){
            $orders = Order::orderBy('created_at', $order)->paginate($pagi);
        }
        if($sortby == 'item_sku'){
            $orders = Order::where('order_status', '=', 'UNPAID')
                    ->orderBy('items', $order)
                    ->paginate($pagi);
        }

        if($request->ajax()){
            return view('order.unpaid', compact('orders', 'payments'))
                ->withPagination($pagi);
        }else{
            return view('order.unpaid', compact('orders', 'payments'))
                ->withPagination($pagi);
        }
    }

    public function dataReady(Request $request)
    {
        $pagi = $request->get('pagination') ?? 100;
        $orders = Order::where('order_status', '=', 'READY_TO_SHIP')->orderBy('created_at', 'DESC')->paginate($pagi);
        $payments = Payment::get();
        //SearchData//
        if(!empty($request->get('ordersn'))){
            $ordersn = $request->ordersn ? $request->ordersn : 0;
            $orders = Order::where('ordersn', 'LIKE', '%'.$ordersn.'%')->paginate(10);
        }
        if(!empty($request->get('name_customer'))){
            $request->session()->put('name_customer', $request->has('name_customer') ? $request->get('name_customer') : ($request->session()->has('ordersn') ? $request->session()->get('name_customer') : ''));
            $orders = Order::where('recipient_address', 'LIKE', '%"name":"%'.$request->session()->get('name_customer').'%"%')->paginate(10);
        }
        if(!empty($request->get('product'))){
            $orders = Order::where('items', 'LIKE', '%"item_name":"%'.$request->get('product').'%"%')->paginate($pagi);
        }
        if(!empty($request->get('item_sku'))){
            $orders = Order::where('items', 'LIKE', '%"item_sku":"%'.$request->get('item_sku').'%"%')->paginate($pagi);
        }
        if(!empty($request->get('payments'))){
            $orders = Order::where('payment_method', 'LIKE', '%'.$request->get('payments').'%')->paginate($pagi);
        }
        if(!empty($request->get('date'))){
            $subStr_Date = explode(' - ',$request->get('date'));
            $startDate = new Carbon($subStr_Date[0]);
            $endDate = new Carbon($subStr_Date[1]);
            $orders = Order::whereBetween('created_at', array($startDate->toDateString(),$endDate->toDateString()))->paginate($pagi);
        }
        //FiterData//
        $sortby = $request->get('sortby') ? $request->get('sortby') : '';
        $order = $request->get('order') ? $request->get('order') : 'DESC';
        if($sortby == 'ordersn'){
            $orders = Order::orderBy('ordersn', $order)->paginate($pagi);
        }
        if($sortby == 'date'){
            $orders = Order::orderBy('created_at', $order)->paginate($pagi);
        }
        if($sortby == 'item_sku'){
            $orders = Order::where('order_status', '=', 'READY_TO_SHIP')
                    ->orderBy('items', $order)
                    ->paginate($pagi);
        }

        if($request->ajax()){
            return view('order.ready', compact('payments', 'orders'))
                ->withPagination($pagi);
        }else{
            return view('order.ready', compact('payments', 'orders'))
                ->withPagination($pagi);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function dataComplete(Request $request)
    {
        $pagi = $request->get('pagination') ?? 100;
        $orders = Order::with(['item'])->where('order_status', '=', 'COMPLETED')->orderBy('created_at', 'DESC')->paginate($pagi);
        $payments = Payment::get();
        //SearchData//
        if(!empty($request->get('ordersn'))){
            $ordersn = $request->ordersn ? $request->ordersn : 0;
            $orders = Order::where('ordersn', 'LIKE', '%'.$ordersn.'%')->paginate($pagi);
        }
        if(!empty($request->get('name_customer'))){
            $request->session()->put('name_customer', $request->has('name_customer') ? $request->get('name_customer') : ($request->session()->has('ordersn') ? $request->session()->get('name_customer') : ''));
            $orders = Order::where('recipient_address', 'LIKE', '%"name":"%'.$request->session()->get('name_customer').'%"%')->paginate($pagi);
        }
        if(!empty($request->get('product'))){
            $orders = Order::where('items', 'LIKE', '%"item_name":"%'.$request->get('product').'%"%')->paginate($pagi);
        }
        if(!empty($request->get('item_sku'))){
            $orders = Order::where('items', 'LIKE', '%"item_sku":"%'.$request->get('item_sku').'%"%')->paginate($pagi);
        }
        if(!empty($request->get('payments'))){
            $orders = Order::where('payment_method', 'LIKE', '%'.$request->get('payments').'%')->paginate($pagi);
        }
        if(!empty($request->get('date'))){
            $subStr_Date = explode(' - ',$request->get('date'));
            $startDate = new Carbon($subStr_Date[0]);
            $endDate = new Carbon($subStr_Date[1]);
            $orders = Order::whereBetween('created_at', array($startDate->toDateString(),$endDate->toDateString()))->paginate($pagi);
        }
        if(!empty($request->get('tag_item_sku'))){
            $orders = Order::where('items', 'LIKE', '%"item_sku":"%'.$request->get('tag_item_sku').'%"%')->paginate($pagi);
        }
        //FiterData//
        $sortby = $request->get('sortby') ? $request->get('sortby') : '';
        $order = $request->get('order') ? $request->get('order') : 'DESC';
        if($sortby == 'ordersn'){
            $orders = Order::orderBy('ordersn', $order)->paginate($pagi);
        }
        if($sortby == 'date'){
            $orders = Order::orderBy('created_at', $order)->paginate($pagi);
        }
        if($sortby == 'item_sku'){
            $orders = Order::where('order_status', '=', 'COMPLETED')
                    ->orderBy('items', $order)
                    ->paginate($pagi);
        }

        if($request->ajax()){
            return view('order.complete', compact('payments', 'orders'))
                ->withPagination($pagi);
        }else{
            return view('order.complete', compact('payments', 'orders'))
                ->withPagination($pagi);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);
        if($order == null){
            return redirect()->route('order.view')->with('error', 'ไม่มีค่า ID นี้');
        }
        return view('order.show', compact('order'));
    }

    public function report(Request $request)
    {
        try {
            $start_date = $request->get('start_date');
            $end_date = $request->get('end_date');
            
            $reports = Order::with('item')
                        ->whereBetween('created_at', [$start_date, $end_date])
                        ->get();

            $count = $reports->count();

            $data = [
                'reports' => $reports,
                'count' => $count,
                'start_date' => $start_date,
                'end_date' => $end_date,
            ];

            $pdf = PDF::loadView('order.report', $data);

            //return $pdf->stream();
            return $pdf->download("DiscountReportPrint.pdf");
        } catch (Exception $exception) {
            return redirect()->route('order.view')->with('error', $exception->getMessage());
        }
    }

    public function updateCountPrint(Request $request)
    {
        $request->validate([
            'ordersn' => 'required'
        ]);

        try {
            $allSn = $request->get('ordersn');
                $order = Order::with('item')->whereIn('ordersn', $allSn)->get();
                $countOrder = Order::with('item')->whereIn('ordersn', $allSn)->count();
                $address = json_decode($order[0]->recipient_address);
                $itemImg = Product::find($order[0]->item->item_id);
                $generatorHTML = new \Picqer\Barcode\BarcodeGeneratorHTML();
                $shop = Shop::find($itemImg->shopid);

                $renderer = new \BaconQrCode\Renderer\Image\Png();
                $renderer->setHeight(150);
                $renderer->setWidth(200);

                $str_qrcode = $order[0]->ordersn.$order[0]->tracking_no.$address->phone;
                $writer = new \BaconQrCode\Writer($renderer);
                $qr_image = base64_encode($writer->writeString($str_qrcode));

                foreach($allSn as $idorder){
                    $updateorder = Order::find($idorder);
                    $updateorder->cum_printout += 1;
                    $updateorder->save();
                }
                // $data = [
                //     'order' => $order,
                //     'itemImg' => $itemImg,
                //     'generatorHTML' => $generatorHTML,
                //     'shop' => $shop,
                //     'qr_image' => $qr_image,
                //     'count' => $countOrder,
                // ];

                // $pdf = PDF::loadView('order.exportdiscount', $data);

                // return $pdf->stream();
                return response()->json(['status' => true, 'order' => $order, 'itemImg' => $itemImg, 'generatorHTML' => $generatorHTML, 'shop' => $shop, 'qr_image' => $qr_image, 'count' => $countOrder]);
        } catch (Exception $exception) {
                return redirect()->route('order.view')->with('error', $exception->getMessage());
        }
    }

    public function tagsGroup(Request $request)
    {
    	$data = [];


        if($request->has('q')){
            $search = $request->q;
            $data = DB::table("orders")
            		->select("ordersn","tracking_no")
            		->where('ordersn','LIKE',"%$search%")
            		->get();
        }


        return response()->json($data);
    }

    public function exportDiscountItem($id)
    {
        try {            
            $order = Order::find($id);
            $items = json_decode($order->items);
            $address = json_decode($order->recipient_address);
            $itemImg = Product::find($items->item_id);
            $generatorHTML = new \Picqer\Barcode\BarcodeGeneratorHTML();
            $shop = Shop::find($itemImg->shopid);

            $renderer = new \BaconQrCode\Renderer\Image\Png();
            $renderer->setHeight(150);
            $renderer->setWidth(200);
      
            $writer = new \BaconQrCode\Writer($renderer);

            $str_qrcode = $order->ordersn.$order->tracking_no.$address->phone;
            $qr_image = base64_encode($writer->writeString($str_qrcode));

            $d = new DNS1D();
            $d->setStorPath(__DIR__."/cache/");

            $data = [
                'order' => $order,
                'image' => $itemImg->images,
                'generator' => $generatorHTML,
                'shop' => $shop,
                'qr_image' => $qr_image,
                'd' => $d,
            ];
            $pdf = PDF::loadView('order.exportdiscount', $data);

            return $pdf->stream();
            //return $pdf->download("Discount".$order->ordersn.".pdf");
        } catch (Exception $exception) {
            return redirect()->route('order.view')->with('error', $exception->getMessage());
        }
    }
    public function barcode( $filepath="", $text="0", $size="20", $orientation="horizontal", $code_type="code128", $print=false, $SizeFactor=1 ) {
        $filepath = (isset($_GET["filepath"])?$_GET["filepath"]:"");
        $text = (isset($_GET["text"])?$_GET["text"]:"0");
        $size = (isset($_GET["size"])?$_GET["size"]:"20");
        $orientation = (isset($_GET["orientation"])?$_GET["orientation"]:"horizontal");
        $code_type = (isset($_GET["codetype"])?$_GET["codetype"]:"code128");
        $print = (isset($_GET["print"])&&$_GET["print"]=='true'?true:false);
        $sizefactor = (isset($_GET["sizefactor"])?$_GET["sizefactor"]:"1");
        // This function call can be copied into your project and can be made from anywhere in your code
        $code_string = "";
        // Translate the $text into barcode the correct $code_type
        if ( in_array(strtolower($code_type), array("code128", "code128b")) ) {
            $chksum = 104;
            // Must not change order of array elements as the checksum depends on the array's key to validate final code
            $code_array = array(" "=>"212222","!"=>"222122","\""=>"222221","#"=>"121223","$"=>"121322","%"=>"131222","&"=>"122213","'"=>"122312","("=>"132212",")"=>"221213","*"=>"221312","+"=>"231212",","=>"112232","-"=>"122132","."=>"122231","/"=>"113222","0"=>"123122","1"=>"123221","2"=>"223211","3"=>"221132","4"=>"221231","5"=>"213212","6"=>"223112","7"=>"312131","8"=>"311222","9"=>"321122",":"=>"321221",";"=>"312212","<"=>"322112","="=>"322211",">"=>"212123","?"=>"212321","@"=>"232121","A"=>"111323","B"=>"131123","C"=>"131321","D"=>"112313","E"=>"132113","F"=>"132311","G"=>"211313","H"=>"231113","I"=>"231311","J"=>"112133","K"=>"112331","L"=>"132131","M"=>"113123","N"=>"113321","O"=>"133121","P"=>"313121","Q"=>"211331","R"=>"231131","S"=>"213113","T"=>"213311","U"=>"213131","V"=>"311123","W"=>"311321","X"=>"331121","Y"=>"312113","Z"=>"312311","["=>"332111","\\"=>"314111","]"=>"221411","^"=>"431111","_"=>"111224","\`"=>"111422","a"=>"121124","b"=>"121421","c"=>"141122","d"=>"141221","e"=>"112214","f"=>"112412","g"=>"122114","h"=>"122411","i"=>"142112","j"=>"142211","k"=>"241211","l"=>"221114","m"=>"413111","n"=>"241112","o"=>"134111","p"=>"111242","q"=>"121142","r"=>"121241","s"=>"114212","t"=>"124112","u"=>"124211","v"=>"411212","w"=>"421112","x"=>"421211","y"=>"212141","z"=>"214121","{"=>"412121","|"=>"111143","}"=>"111341","~"=>"131141","DEL"=>"114113","FNC 3"=>"114311","FNC 2"=>"411113","SHIFT"=>"411311","CODE C"=>"113141","FNC 4"=>"114131","CODE A"=>"311141","FNC 1"=>"411131","Start A"=>"211412","Start B"=>"211214","Start C"=>"211232","Stop"=>"2331112");
            $code_keys = array_keys($code_array);
            $code_values = array_flip($code_keys);
            for ( $X = 1; $X <= strlen($text); $X++ ) {
                $activeKey = substr( $text, ($X-1), 1);
                $code_string .= $code_array[$activeKey];
                $chksum=($chksum + ($code_values[$activeKey] * $X));
            }
            $code_string .= $code_array[$code_keys[($chksum - (intval($chksum / 103) * 103))]];
            $code_string = "211214" . $code_string . "2331112";
        } elseif ( strtolower($code_type) == "code128a" ) {
            $chksum = 103;
            $text = strtoupper($text); // Code 128A doesn't support lower case
            // Must not change order of array elements as the checksum depends on the array's key to validate final code
            $code_array = array(" "=>"212222","!"=>"222122","\""=>"222221","#"=>"121223","$"=>"121322","%"=>"131222","&"=>"122213","'"=>"122312","("=>"132212",")"=>"221213","*"=>"221312","+"=>"231212",","=>"112232","-"=>"122132","."=>"122231","/"=>"113222","0"=>"123122","1"=>"123221","2"=>"223211","3"=>"221132","4"=>"221231","5"=>"213212","6"=>"223112","7"=>"312131","8"=>"311222","9"=>"321122",":"=>"321221",";"=>"312212","<"=>"322112","="=>"322211",">"=>"212123","?"=>"212321","@"=>"232121","A"=>"111323","B"=>"131123","C"=>"131321","D"=>"112313","E"=>"132113","F"=>"132311","G"=>"211313","H"=>"231113","I"=>"231311","J"=>"112133","K"=>"112331","L"=>"132131","M"=>"113123","N"=>"113321","O"=>"133121","P"=>"313121","Q"=>"211331","R"=>"231131","S"=>"213113","T"=>"213311","U"=>"213131","V"=>"311123","W"=>"311321","X"=>"331121","Y"=>"312113","Z"=>"312311","["=>"332111","\\"=>"314111","]"=>"221411","^"=>"431111","_"=>"111224","NUL"=>"111422","SOH"=>"121124","STX"=>"121421","ETX"=>"141122","EOT"=>"141221","ENQ"=>"112214","ACK"=>"112412","BEL"=>"122114","BS"=>"122411","HT"=>"142112","LF"=>"142211","VT"=>"241211","FF"=>"221114","CR"=>"413111","SO"=>"241112","SI"=>"134111","DLE"=>"111242","DC1"=>"121142","DC2"=>"121241","DC3"=>"114212","DC4"=>"124112","NAK"=>"124211","SYN"=>"411212","ETB"=>"421112","CAN"=>"421211","EM"=>"212141","SUB"=>"214121","ESC"=>"412121","FS"=>"111143","GS"=>"111341","RS"=>"131141","US"=>"114113","FNC 3"=>"114311","FNC 2"=>"411113","SHIFT"=>"411311","CODE C"=>"113141","CODE B"=>"114131","FNC 4"=>"311141","FNC 1"=>"411131","Start A"=>"211412","Start B"=>"211214","Start C"=>"211232","Stop"=>"2331112");
            $code_keys = array_keys($code_array);
            $code_values = array_flip($code_keys);
            for ( $X = 1; $X <= strlen($text); $X++ ) {
                $activeKey = substr( $text, ($X-1), 1);
                $code_string .= $code_array[$activeKey];
                $chksum=($chksum + ($code_values[$activeKey] * $X));
            }
            $code_string .= $code_array[$code_keys[($chksum - (intval($chksum / 103) * 103))]];
            $code_string = "211412" . $code_string . "2331112";
        } elseif ( strtolower($code_type) == "code39" ) {
            $code_array = array("0"=>"111221211","1"=>"211211112","2"=>"112211112","3"=>"212211111","4"=>"111221112","5"=>"211221111","6"=>"112221111","7"=>"111211212","8"=>"211211211","9"=>"112211211","A"=>"211112112","B"=>"112112112","C"=>"212112111","D"=>"111122112","E"=>"211122111","F"=>"112122111","G"=>"111112212","H"=>"211112211","I"=>"112112211","J"=>"111122211","K"=>"211111122","L"=>"112111122","M"=>"212111121","N"=>"111121122","O"=>"211121121","P"=>"112121121","Q"=>"111111222","R"=>"211111221","S"=>"112111221","T"=>"111121221","U"=>"221111112","V"=>"122111112","W"=>"222111111","X"=>"121121112","Y"=>"221121111","Z"=>"122121111","-"=>"121111212","."=>"221111211"," "=>"122111211","$"=>"121212111","/"=>"121211121","+"=>"121112121","%"=>"111212121","*"=>"121121211");
            // Convert to uppercase
            $upper_text = strtoupper($text);
            for ( $X = 1; $X<=strlen($upper_text); $X++ ) {
                $code_string .= $code_array[substr( $upper_text, ($X-1), 1)] . "1";
            }
            $code_string = "1211212111" . $code_string . "121121211";
        } elseif ( strtolower($code_type) == "code25" ) {
            $code_array1 = array("1","2","3","4","5","6","7","8","9","0");
            $code_array2 = array("3-1-1-1-3","1-3-1-1-3","3-3-1-1-1","1-1-3-1-3","3-1-3-1-1","1-3-3-1-1","1-1-1-3-3","3-1-1-3-1","1-3-1-3-1","1-1-3-3-1");
            for ( $X = 1; $X <= strlen($text); $X++ ) {
                for ( $Y = 0; $Y < count($code_array1); $Y++ ) {
                    if ( substr($text, ($X-1), 1) == $code_array1[$Y] )
                        $temp[$X] = $code_array2[$Y];
                }
            }
            for ( $X=1; $X<=strlen($text); $X+=2 ) {
                if ( isset($temp[$X]) && isset($temp[($X + 1)]) ) {
                    $temp1 = explode( "-", $temp[$X] );
                    $temp2 = explode( "-", $temp[($X + 1)] );
                    for ( $Y = 0; $Y < count($temp1); $Y++ )
                        $code_string .= $temp1[$Y] . $temp2[$Y];
                }
            }
            $code_string = "1111" . $code_string . "311";
        } elseif ( strtolower($code_type) == "codabar" ) {
            $code_array1 = array("1","2","3","4","5","6","7","8","9","0","-","$",":","/",".","+","A","B","C","D");
            $code_array2 = array("1111221","1112112","2211111","1121121","2111121","1211112","1211211","1221111","2112111","1111122","1112211","1122111","2111212","2121112","2121211","1121212","1122121","1212112","1112122","1112221");
            // Convert to uppercase
            $upper_text = strtoupper($text);
            for ( $X = 1; $X<=strlen($upper_text); $X++ ) {
                for ( $Y = 0; $Y<count($code_array1); $Y++ ) {
                    if ( substr($upper_text, ($X-1), 1) == $code_array1[$Y] )
                        $code_string .= $code_array2[$Y] . "1";
                }
            }
            $code_string = "11221211" . $code_string . "1122121";
        }
        // Pad the edges of the barcode
        $code_length = 20;
        if ($print) {
            $text_height = 30;
        } else {
            $text_height = 0;
        }
        
        for ( $i=1; $i <= strlen($code_string); $i++ ){
            $code_length = $code_length + (integer)(substr($code_string,($i-1),1));
            }
        if ( strtolower($orientation) == "horizontal" ) {
            $img_width = $code_length*$SizeFactor;
            $img_height = $size;
        } else {
            $img_width = $size;
            $img_height = $code_length*$SizeFactor;
        }
        $image = imagecreate($img_width, $img_height + $text_height);
        $black = imagecolorallocate ($image, 0, 0, 0);
        $white = imagecolorallocate ($image, 255, 255, 255);
        imagefill( $image, 0, 0, $white );
        if ( $print ) {
            imagestring($image, 5, 31, $img_height, $text, $black );
        }
        $location = 10;
        for ( $position = 1 ; $position <= strlen($code_string); $position++ ) {
            $cur_size = $location + ( substr($code_string, ($position-1), 1) );
            if ( strtolower($orientation) == "horizontal" )
                imagefilledrectangle( $image, $location*$SizeFactor, 0, $cur_size*$SizeFactor, $img_height, ($position % 2 == 0 ? $white : $black) );
            else
                imagefilledrectangle( $image, 0, $location*$SizeFactor, $img_width, $cur_size*$SizeFactor, ($position % 2 == 0 ? $white : $black) );
            $location = $cur_size;
        }
        
        // Draw barcode to the screen or save in a file
        if ( $filepath=="" ) {
            header ('Content-type: image/png');
            imagepng($image);
            imagedestroy($image);
        } else {
            imagepng($image,$filepath);
            imagedestroy($image);		
        }
    }
}
