<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Role;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $roles = Role::get();
        $users = User::with('role')->paginate(10);
        if(!empty($request->get('name'))){
            $users = User::where('name', 'LIKE', '%'.$request->get('name').'%')->paginate(10);
        }
        if(!empty($request->get('email'))){
            $users = User::where('email', 'LIKE', '%'.$request->get('email').'%')->paginate(10);
        }
        if(!empty($request->get('role'))){
            $users = User::where('id_role', 'LIKE', '%'.$request->get('role').'%')->paginate(10);
        }
        if(!empty($request->get('active'))){
            $users = User::where('active', 'LIKE', '%'.$request->get('active').'%')->paginate(10);
        }

        return view('user.index', compact('users', 'roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::get();

        return view('user.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
        ],[
            'name.required' => 'กรุณากรอกชื่อ-นามสกุล/ชื่อร้านค้า',
            'email.required' => 'กรุณากรอกอีเมล์',
            'password.required' => 'กรุณากรอกรหัสผ่าน',
        ]);

        try {
           $user = new User;
           $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->password = Hash::make($request->input('password'));
            $user->id_role = $request->input('role');
            $user->active = "1";
            $user->portrait = "1";
            $user->save();

            return redirect()->route('user.view')->with('success', 'บันทึกข้อมูลเรียบร้อยแล้ว');
        } catch (\Exception $exception) {
            return redirect()->route('user.view')->with('error', $exception->getMessage());
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::get();
        $user = User::find($id);
        if($user == null){
            return redirect()->route('user.view')->with('error', 'ไม่มีค่า ID นี้');
        }
        return view('user.edit', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $user = User::find($id);
            $user->name = $request->input('name');
             $user->email = $request->input('email');
             if(!empty($request->get('password'))) {
                $user->password = Hash::make($request->get('password'));
            } else {
                $user->password = Auth::user()->password;
            }
             $user->id_role = $request->input('role');
             $user->save();

             return redirect()->route('user.view')->with('success', 'แก้ไขข้อมูลเรียบร้อยแล้ว');
         } catch (\Exception $exception) {
             return redirect()->route('user.view')->with('error', $exception->getMessage());
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function disabled(Request $request)
    {
        $request->validate([
            'id' => 'required'
        ]);

        try {
            $user = User::find($request->id);
            $user->active = "2";
            $user->save();
            return response()->json(['message'=>"ปิดการใช้งานเรียบร้อยแล้ว.", 'status' => true]);

        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function enabled(Request $request)
    {
        $request->validate([
            'id' => 'required'
        ]);

        try {
            $user = User::find($request->id);
            $user->active = "1";
            $user->save();
            return response()->json(['message'=>"ปิดการใช้งานเรียบร้อยแล้ว.", 'status' => true]);

        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    
    public function editProfile($id)
    {
        $roles = Role::get();
        $user = User::find($id);
        return view('user.profile', compact('user', 'roles'));
    }

    public function updateProfile(Request $request,$id)
    {
        try {
            $user = User::find($id);
            $user->name = $request->input('name');
            $user->email = $request->input('email');
             if(!empty($request->get('password'))) {
                $user->password = Hash::make($request->get('password'));
            } else {
                $user->password = Auth::user()->password;
            }
             $user->id_role = $request->input('role');
             $user->save();

             return redirect()->route('user.editProfile', $user->id)->with('success', 'แก้ไขข้อมูลเรียบร้อยแล้ว');
         } catch (\Exception $exception) {
             return redirect()->route('user.view')->with('error', $exception->getMessage());
         }
    }
}
