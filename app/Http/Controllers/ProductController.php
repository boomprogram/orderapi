<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = Product::with('category')
                ->orderBy('category_id', 'DESC')
                ->paginate(20);
        if(!empty($request->get('product'))){
            $products = Product::where('name', 'LIKE', '%'.$request->get('product').'%')->paginate(20);
        }
        if(!empty($request->get('status'))){
            $products = Product::where('status', 'LIKE', '%'.$request->get('status').'%')->paginate(20);
        }
        if(!empty($request->get('category'))){
            $products = Product::where('category_id', 'LIKE', '%'.$request->get('category').'%')->paginate(20);
        }
        $catagories = Category::with('product')->get();
        return view('product.index', compact('products', 'catagories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $catagories = Category::with('product')->get();

        return view('product.create', compact('catagories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'catagory' => 'required',
            'name' => 'required',
            'description' => 'required',
        ],[
            'catagory.required' => 'กรุณาเลือกหมวดหมู่สินค้า',
            'name.required' => 'กรุณาระบุชื่อสินค้า',
            'description.required' => 'กรุณาระบุรายละเอียดสินค้า'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        // dd($data);
        return view('product.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
