<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    protected $primaryKey = "shop_id";
    protected $table = "shop";
    protected $fillable = ['shop_id', 'shop_name', 'shop_value', 'active'];
}
