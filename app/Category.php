<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $primaryKey = "category_id";
    protected $table = "categories";
    protected $fillable = [
        'category_id',
        'parent_id',
        'category_name',
        'has_children'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'category_id');
    }
}
