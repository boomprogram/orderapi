<?php
/**
 * Custom Function Helper
 */
function remove_Quote($value){
    $str = str_replace(" " , "",$value);
    return $str;
}
function remove_bracketL($value){
    $str = str_replace("[", "",$value);
    return $str;
}
function remove_bracketR($value){
    $str = str_replace("]", "",$value);
    return $str;
}
function ConvertDate($value){
    $thai_month_arr=array(
        "00"=>"",
        "01"=>"มกราคม",
        "02"=>"กุมภาพันธ์",
        "03"=>"มีนาคม",
        "04"=>"เมษายน",
        "05"=>"พฤษภาคม",
        "06"=>"มิถุนายน",
        "07"=>"กรกฎาคม",
        "08"=>"สิงหาคม",
        "09"=>"กันยายน",
        "10"=>"ตุลาคม",
        "11"=>"พฤศจิกายน",
        "12"=>"ธันวาคม"
    );

    $ex_value = explode(" ",$value);
    $str_date = $ex_value[0];
    //2018-02-30
    $ex_str = explode("-",$str_date);
    $convert_strmonth = $ex_str[1];
    $convert_strmonth = $thai_month_arr[$convert_strmonth];
    $newyear = $ex_str[0]+543;
    $newDate = $ex_str[2]."-".$convert_strmonth."-".$newyear;
    //08:00
    $str_time = $ex_value[1];
    $newTime = substr($str_time,0,-3);
    $convertTime = $newDate." ".$newTime."";

    return $convertTime;
}
function ConvertDate1($value){
    $thai_month_arr=array(
        "00"=>"",
        "01"=>"มกราคม",
        "02"=>"กุมภาพันธ์",
        "03"=>"มีนาคม",
        "04"=>"เมษายน",
        "05"=>"พฤษภาคม",
        "06"=>"มิถุนายน",
        "07"=>"กรกฎาคม",
        "08"=>"สิงหาคม",
        "09"=>"กันยายน",
        "10"=>"ตุลาคม",
        "11"=>"พฤศจิกายน",
        "12"=>"ธันวาคม"
    );
    $ex_value = explode(" ",$value);
    $str_date = $ex_value[0];

    $ex_str = explode("-",$str_date);
    $convert_strmonth = $ex_str[1];
    $convert_strmonth = $thai_month_arr[$convert_strmonth];
    $newyear = $ex_str[0]+543;
    $newDate = $ex_str[2]."-".$convert_strmonth."-".$newyear;

    return $newDate;
}
