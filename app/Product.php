<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $incrementing = false;
    protected $primaryKey = "item_id";
    protected $table = "items";
    protected $fillable = ['item_id',
                        'logistics',
                        'original_price',
                        'package_width',
                        'cmt_count',
                        'weight',
                        'shopid',
                        'currency',
                        'likes',
                        'images',
                        'days_to_ship',
                        'package_length',
                        'stock',
                        'status',
                        'description',
                        'views',
                        'price',
                        'sales',
                        'discount_id',
                        'wholesales',
                        'condition',
                        'package_height',
                        'name',
                        'rating_star',
                        'item_sku',
                        'variations',
                        'size_chart',
                        'has_variation',
                        'attributes',
                        'category_id',
                        ];

    public function Category()
    {
        return $this->belongsTo(Category::Class, 'category_id');
    }
}
