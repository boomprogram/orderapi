<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="token" content="{{ csrf_token() }}">
    <title>ระบบจัดการคำสั่งซื้อสินค้า MarketPlaces</title>
    @include('layouts.inc-stylesheets')
    @yield('stylesheets')
</head>
<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">
    @include('layouts.inc-menu')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
        @yield('content')
  </div>
  @include('layouts.inc-scripts')
  @yield('scripts')
</div>
</body>
</html>
