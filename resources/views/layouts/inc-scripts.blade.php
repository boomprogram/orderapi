<!-- jQuery 2.2.3 -->
<script src="{{ asset('js/jquery-2.2.3.min.js')}}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset('js/bootstrap.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset('js/icheck.js') }}"></script>
<!-- AdminLTE -->
<script src="{{ asset('js/app.min.js') }}"></script>
<!-- BootBox -->
<script src="{{ asset('js/bootbox.min.js') }}"></script>
<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>

<script>
  $(function () {
//     $('input').iCheck({
//       checkboxClass: 'icheckbox_square-blue',
//       radioClass: 'iradio_square-blue',
//       increaseArea: '20%' // optional
//     });
  });
  function shinkData(shopId,partnerId,secretId){
    bootbox.confirm({
        message: "ต้องการอัพเดตข้อมูลร้าน?",
        buttons: {
            confirm: {
                label: 'ใช่',
                className: 'btn-success'
            },
            cancel: {
                label: 'ไม่',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if(result) {
                $.ajax({
                        type: "POST",
                        url: "{{ route('config.syncdata') }}",
                        dataType: 'JSON',
                        data: {
                            _token: "{{ csrf_token() }}",
                            shopid : shopId,
                            partner_id : partnerId,
                            secret : secretId,
                        },
                        beforeSend: function(){
                            bootbox.dialog({ 
                                message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> <br><h3>กำลังอัพเดตข้อมูลร้านของคุณ กรุณารอสักครู่...</h3></div>',
                                closeButton: false 
                            });
                        },
                        success: function(data){
                            if(data.status == true){
                                bootbox.alert({
                                    message: "อัพเดตข้อมูลร้านเรียบร้อยแล้ว",
                                    callback: function () {
                                        window.location.reload();
                                    }
                                })
                            }
                        },
                        error: function(jqXHR, exception){
                            var msg = '';
                            if (jqXHR.status === 0) {
                                msg = 'Not connect.\n Verify Network.';
                            } else if (jqXHR.status == 404) {
                                msg = 'Requested page not found. [404]';
                            } else if (jqXHR.status == 500) {
                                msg = 'Error 500.';
                            } else if (exception === 'parsererror') {
                                msg = 'Requested JSON parse failed.';
                            } else if (exception === 'timeout') {
                                msg = 'Time out error.';
                            } else if (exception === 'abort') {
                                msg = 'Ajax request aborted.';
                            } else {
                                msg = 'Uncaught Error.\n' + jqXHR.responseText;
                            }
                            alert(msg);
                        }
                });
            }
        }
    });
    }
</script>
