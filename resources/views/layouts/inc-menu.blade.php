<header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>M</b>P</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Market</b>Place</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              @php
                  $countOrder = App\Order::count();
              @endphp
                    <!-- Notifications Menu -->
                <li class="dropdown notifications-menu">
                      <!-- Menu toggle button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-warning">{{ $countOrder }}</span>
                    </a>
                    </li>
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                      <!-- Menu Toggle Button -->
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- The user image in the navbar-->
                        <img src="{{ asset('img/no_img.png') }}" class="user-image" alt="User Image">
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs">{{ Auth::user()->name }}</span>
                      </a>
                      <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header">
                          <img src="{{ asset('img/no_img.png') }}" class="img-circle" alt="User Image">

                          <p>
                            {{ Auth::user()->name }}
                            <small>{{ Auth::user()->role->name }}</small>
                          </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                          <div class="pull-left">
                            <a href="{{ route('user.editProfile', Auth::user()->id) }}" class="btn btn-default btn-flat">ตั้งค่าบัญชี</a>
                          </div>
                          <div class="pull-right">
                            <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="btn btn-default btn-flat">ออกจากระบบ</a>
                          </div>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                      </ul>
                </li>
            </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <ul class="sidebar-menu">
                @if(Auth::user()->id_role == 1 || Auth::user()->id_role == 2)
                <li class="treeview">
                    <a href="{{ route('home') }}"><i class="fa fa-dashboard"></i>&nbsp;ข้อมูลรวม</a>
                </li>
                <li class="treeview {{ Request::is( 'products', 'products/create', 'categories') ? 'active' : '' }}">
                <a href="#"><i class="fa fa-archive"></i>&nbsp;ผลิตภัณฑ์
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ Request::is( 'products') ? 'active' : '' }}"><a href="{{ route('product.view') }}"><i class="fa fa-circle-o"></i> รายการสินค้า</a></li>
                    <li class="{{ Request::is( 'products/create') ? 'active' : '' }}"><a href="{{ route('product.create') }}"><i class="fa fa-circle-o"></i> เพิ่มสินค้า</a></li>
                    <li class="{{ Request::is( 'categories') ? 'active' : '' }}"><a href="{{ route('category.view') }}"><i class="fa fa-circle-o"></i> หมวดสินค้า</a></li>
                </ul>
                </li>
                <li class="treeview {{ Request::is( 'orders', 'orders/unpaid', 'orders/ready', 'orders/complete') ? 'active' : '' }}">
                    <a href="#"><i class="fa fa-cart-plus"></i>&nbsp;คำสั่งซื้อ
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{{ Request::is( 'orders', 'orders/unpaid', 'orders/ready', 'orders/complete') ? 'active' : '' }}"><a href="{{ route('order.view') }}"><i class="fa fa-circle-o"></i> รายการคำสั่งซื้อ</a></li>
                    </ul>
                </li>
                @endif
                @if(Auth::user()->id_role == 1)
                <li class="treeview {{ Request::is( 'users', 'users/create') ? 'active' : '' }}">
                    <a href="#"><i class="fa fa-users"></i>&nbsp;บัญชีผู้ใช้งาน
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{{ Request::is( 'users') ? 'active' : '' }}"><a href="{{ route('user.view') }}"><i class="fa fa-circle-o"></i> รายการผู้ใช้งาน</a></li>
                        <li class="{{ Request::is( 'users/create') ? 'active' : '' }}"><a href="{{ route('user.create') }}"><i class="fa fa-circle-o"></i> สร้างผู้ใช้งาน</a></li>
                    </ul>
                </li>
                <li class="treeview {{ Request::is( 'shops') ? 'active' : '' }}">
                    <a href="#"><i class="fa fa-gear"></i>&nbsp;ตั้งค่าระบบ
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{{ Request::is( 'shops') ? 'active' : '' }}"><a href="{{ route('shop.view') }}"><i class="fa fa-circle-o"></i> ตั้งค่าร้าน</a></li>
                    </ul>
                </li>
                @endif
          </ul>
        </section>
        <!-- /.sidebar -->
        <center><button type="button" class="btn bg-orange margin" onclick="shinkData('52591873','840997','1f30a88d84b5e4d2309307e799ff20079a4b30163c5e3d652203f0386eeb266e');"><i class="fa fa-refresh"></i>&nbsp;อัพเดตข้อมูลร้าน</button></center>
    </aside>
