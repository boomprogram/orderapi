@extends('layouts.layout')

@section('content')
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
            ตั้งค่าบัญชี
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-users"></i> บัญชีผู้ใช้งาน</a></li>
                <li class="active">ตั้งค่าบัญชี</li>
            </ol>
        </section>
        <section class="content">
            @include('flash_msg')
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-body box-profile">
                                <img class="profile-user-img img-responsive img-circle" src="{{ asset('img/no_img.png') }}" alt="User profile picture">
                                <h3 class="profile-username text-center">{{ $user->name }}</h3>
                                <p class="text-muted text-center">{{ $user->role->name }}</p>
                                <form class="form-horizontal" action="{{ route('user.updateProfile', $user->id) }}" method="get">
                                    <div class="form-group">
                                        <label for="inputName" class="col-sm-2 control-label">ชื่อ-นามสกุล/ชื่อร้านค้า</label>

                                        <div class="col-sm-10">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{ $user->name }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail" class="col-sm-2 control-label">อีเมล์แอดเดรส</label>

                                        <div class="col-sm-10">
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{ $user->email }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="password" class="col-sm-2 control-label">รหัสผ่าน</label>
                                        <div class="col-sm-10">
                                            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="role" class="col-sm-2 control-label">สิทธิ์ผู้ใช้งาน</label>
                                        <div class="col-sm-10">
                                        <select class="form-control" name="role" id="role" disabled>
                                            <option value="">--- กรุณาเลือก ---</option>
                                            @foreach ($roles as $role)
                                                <option value="{{ $role->id }}" {{($user->id_role == $role->id) ? 'selected' : '' }}>{{ $role->name }}</option>
                                            @endforeach
                                        </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="">
                                            <center><button type="submit" class="btn btn-danger">แก้ไขข้อมูล</button></center>
                                        </div>
                                    </div>
                                    <input type="hidden" name="role" value="{{ $user->id_role }}">
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>


@endsection