@extends('layouts.layout')

@section('content')
@include('flash_msg')
        <!-- Content Header (Page header) -->
        <section class="content-header">
                <h1>
                แก้ไขบัญชีผู้ใช้งาน
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-users"></i> บัญชีผู้ใช้งาน</a></li>
                    <li class="active">แก้ไขบัญชีผู้ใช้งาน</li>
                </ol>
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="col-md-6">
                <div class="box box-success">
                <form role="form" action="{{ route('user.update',$user->id) }}" method="POST">
                        @csrf
                    <div class="box-body">
                            <div class="form-group">
                                <label for="name">ชื่อ-นามสกุล/ชื่อร้านค้า</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{ $user->name }}">
                            </div>
                            <div class="form-group">
                                <label for="email">อีเมล์แอดเดรส</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" value="{{ $user->email }}">
                            </div>
                            <div class="form-group">
                                <label for="password">รหัสผ่าน</label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <label for="role">สิทธิ์ผู้ใช้งาน</label>
                                <select class="form-control" name="role" id="role">
                                    <option value="">--- กรุณาเลือก ---</option>
                                    @foreach ($roles as $role)
                                        <option value="{{ $role->id }}" {{($user->id_role == $role->id) ? 'selected' : '' }}>{{ $role->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success">แก้ไขข้อมูล</button>
                    </div>
                </form>
                </div>
                </div>
            </section>

@endsection
