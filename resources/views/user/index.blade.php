@extends('layouts.layout')

@section('content')
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
            รายการบัญชีผู้ใช้งาน
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-users"></i> บัญชีผู้ใช้งาน</a></li>
                <li class="active">รายการบัญชีผู้ใช้งาน</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            @include('flash_msg')
            <div class="box box-primary">
                <div class="box-body">
                    <form id="user-search-form" action="{{ route('user.view') }}" method="GET">
                        <div class="col-md-5">
                            <input type="text" class="form-control" name="name" id="name" placeholder="ชื่อ-นามสกุล">
                        </div>
                        <div class="col-md-5">
                            <input type="text" class="form-control" name="email" id="email" placeholder="อีเมล์">
                        </div>
                        <div class="col-md-2">
                        </div>
                        <div class="col-md-5">
                            <select class="form-control" name="role" id="role" onchange="user_submit()">
                                <option value="">--- สิทธิ์ผู้ใช้งาน ---</option>
                                @foreach ($roles as $role)
                                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-5">
                            <select class="form-control" name="active" id="active" onchange="user_submit()">
                                <option value="">--- สถานะผู้ใช้งาน ---</option>
                                <option value="1">ใช้งาน</option>
                                <option value="2">ไม่ได้ใช้งาน</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-success">
                                <span class="fa fa-search"> ค้นหา
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="box box-default">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="6%">ลำดับที่</th>
                                <th>ชื่อผู้ใช้งาน</th>
                                <th width="30%">ชื่อ-นามสกุล</th>
                                <th>สิทธิ์ผู้ใช้งาน</th>
                                <th>สถานะผู้ใช้งาน</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($users) == 0)
                                <tr>
                                    <td colspan="6"><center>--- ไม่มีรายการในตอนนี้ ---</center></td>
                                </tr>
                            @else
                                @foreach ($users as $key => $user)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->role->name }}</td>
                                    @php
                                        $colorActive;
                                        $functionActive;
                                        $iconActive;
                                        if($user->active == 1){
                                            $txtActive = "ใช้งาน";
                                            $colorActive = "danger";
                                            $functionActive = "disabledUser";
                                            $iconActive = "lock";
                                        }elseif($user->active == 2){
                                            $txtActive = "ไม่ได้ใช้งาน";
                                            $colorActive = "warning";
                                            $functionActive = "enabledUser";
                                            $iconActive = "unlock";
                                        }
                                    @endphp
                                    <td>{{ $txtActive }}</td>
                                    <td><a href="{{ route('user.edit',$user->id) }}" class="btn-sm btn-primary"><i class="fa fa-pencil"></i></a><a href="#" onclick="{{ $functionActive }}({{ $user->id }})" class="btn-sm btn-{{ $colorActive }}"><i class="fa fa-{{ $iconActive }}"></i></a></td>
                                </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                    <p align="right">
                        {{ $users->appends(Request::except('page'))->links() }}
                    </p>
                </div>
            </div>
        </section>
@endsection

@section('scripts')
<script>
    function disabledUser(id){
		bootbox.confirm({
        message: "ต้องการปิดผู้ใช้งานนี้ ?",
        buttons: {
            confirm: {
                label: 'ตกลง',
                className: 'btn-success'
            },
            cancel: {
                label: 'ยกเลิก',
                className: 'btn-danger'
            }
        },
        callback: function(result) {
			if(result) {
                $.ajax({
                    type: "POST",
                    url: "{{ route('user.disabled') }}",
                    dataType: 'JSON',
                    data: { id : id,
                      _token: "{{ csrf_token() }}",
                    },
                    success: function(data){
                        if(data.status == true){
                            alert('ปิดการใช้งานเรียบร้อยแล้ว')
                            window.location.reload();
                        }
                    }
                });
			}
        }
		});
    }
    function enabledUser(id){
        bootbox.confirm({
        message: "ต้องการเปิดผู้ใช้งานนี้ ?",
        buttons: {
            confirm: {
                label: 'ตกลง',
                className: 'btn-success'
            },
            cancel: {
                label: 'ยกเลิก',
                className: 'btn-danger'
            }
        },
        callback : function(result){
            if(result){
                $.ajax({
                    type: "POST",
                    url: "{{ route('user.enabled') }}",
                    dataType: 'JSON',
                    data: { id : id,
                        _token: "{{ csrf_token() }}",
                    },
                    success: function(data){
                        if(data.status == true){
                            alert('เปิดการใช้งานเรียบร้อยแล้ว')
                            window.location.reload();
                        }
                    }
                })
            }
        }
        });
    }

    function user_submit(){
        document.getElementById("user-search-form").submit();
    }
</script>
@endsection
