@extends('layouts.layout')

@section('content')
    <section class="content-header">
        <h1>
        ข้อมูลผลิตภัณฑ์
        </h1>
        <ol class="breadcrumb">
            <li><a href="#">ข้อมูลผลิตภัณฑ์</li>
            <li class="active"><a href="#"><i class="fa fa-dashboard"></i> เพิ่มสินค้า</a></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        @include('flash_msg')
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">เพิ่มสินค้า</h3>
            </div>
            <div class="box-body">
                <form class="form-horizontal" action="{{ route('product.save') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="catagory" class="col-sm-2 control-label">หมวดสินค้า :</label>
                        <div class="col-sm-5">
                            <select class="form-control" name="catagory" id="catagory">
                                <option value="">--- กรุณาเลือก ---</option>
                                @foreach ($catagories as $catagory)
                                    <option value="{{ $catagory->category_id }}">{{ $catagory->category_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">ชื่อสินค้า :</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="name" id="name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="desciption" class="col-sm-2 control-label">รายละเอียดสินค้า :</label>
                        <div class="col-sm-10">
                            <textarea id="editor1" name="editor1" rows="10" cols="80"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="price" class="col-sm-2 control-label">ราคา :</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="price" id="price">
                        </div>
                        <label for="stock" class="col-sm-2 control-label">สต็อคสินค้า :</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="stock" id="stock">
                        </div>
                        <label for="item_sku" class="col-sm-2 control-label">รหัสเรียกสินค้า :</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="item_sku" id="item_sku">
                        </div>
                    </div>
                    <div class="box-footer">
                        <center><button type="submit" class="btn btn-success">บันทึกข้อมูล</button></center>
                    </div>
                </form>
            </div>
        </div>
    </section>

@endsection
@section('scripts')
<script>
$(function () {
    CKEDITOR.replace('editor1');
    $(".textarea").wysihtml5();
});
</script>
@endsection
