@extends('layouts.layout')

@section('content')
    <section class="content-header">
        <h1>
        ข้อมูลผลิตภัณฑ์
        </h1>
        <ol class="breadcrumb">
            <li class="active"><a href="#"><i class="fa fa-dashboard"></i> ข้อมูลผลิตภัณฑ์</a></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box box-primary">
            <div class="box-body">
                <form id="product-search-form" action="{{ route('product.view') }}" method="GET">
                    <div class="col-md-5">
                        <input type="text" class="form-control" name="product" id="product" placeholder="ชื่อสินค้า">
                    </div>
                    <div class="col-md-5">
                        <select class="form-control" name="status" id="status" onchange="product_submit()">
                            <option value="">--- สถานะสินค้า ---</option>
                            <option value="NORMAL">NORMAL</option>
                            <option value="BANNED">BANNED</option>
                            <option value="UNLIST">UNLIST</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                    </div>

                    <div class="col-md-5">
                        <select class="form-control" name="category" id="category" onchange="product_submit()">
                            <option value="">--- หมวดหมู่สินค้า ---</option>
                            @foreach ($catagories as $catagory)
                            <option value="{{ $catagory->category_id }}">{{ $catagory->category_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-5">
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-success">
                            <span class="fa fa-search"> ค้นหา
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">รายการสินค้า</h3>
            </div>
            <div class="box-body table-responsive no-padding">
                <div align="right">
                    {{ $products->links() }}
                </div>
                <table id="tableProduct" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th width="50px">ชื่อ</th>
                            <th width="20px">รหัสเก็บสินค้า</th>
                            <th>สร้าง</th>
                            <th>ราคาขายปลีก</th>
                            <th>ราคาขาย</th>
                            <th>พร้อมใช้</th>
                            <th>การมองเห็นสินค้า</th>
                            <th>โปรโมชั่นที่ใช้งานอยู่</th>
                            <th>ดำเนินการ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($products as $key => $product)
                            <tr>
                                <td style="width:50px;">{{ $product->name }}</td>
                                <td>{{ $product->item_sku }}</td>
                                <td>{{ ConvertDate1($product->created_at) }}</td>
                                <td>{{ number_format($product->original_price, 2, '.', ',') }}</td>
                                <td>{{ number_format($product->price, 2, '.', ',') }}</td>
                                <td>{{ $product->stock }}</td>
                                <td>{{ $product->has_variation }}</td>
                                <td>&nbsp;</td>
                                <td><center><a href="{{ route('product.show', $product['item_id'])}}" class="btn-sm btn-primary"><i class="fa fa-eye"></i></a></center></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div align="right">
                    {{ $products->appends(Request::except('page'))->links() }}
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </section>
@endsection

@section('scripts')
<script>
    function product_submit(){
        document.getElementById("product-search-form").submit();
    }
</script>
@endsection
