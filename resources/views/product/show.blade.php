@extends('layouts.layout')

@section('content')

    <section class="content-header">
        <h1>
        ข้อมูลสินค้า
        </h1>
        <ol class="breadcrumb">
            <li class="active"><a href="#"><i class="fa fa-dashboard"></i> ข้อมูลสินค้า</a></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $product->name }}</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-5">
                            @php
                                $getImg = json_decode($product->images);
                            @endphp
                            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    @foreach ($getImg as $key => $item)
                                        <li data-target="#carousel-example-generic" data-slide-to="{{ $item }}" class="@if($key == 0) active @endif"></li>
                                    @endforeach
                                </ol>
                                <div class="carousel-inner">
                                    @foreach ($getImg as $key => $item)
                                    <div class="item @if($key == 0) active @endif">
                                        <img src="{{ remove_bracketR(remove_bracketL($item)) }}" alt="{{ remove_bracketR(remove_bracketL($item)) }}">
                                    </div>
                                    @endforeach
                                    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                      <span class="fa fa-angle-left"></span>
                                    </a>
                                    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                      <span class="fa fa-angle-right"></span>
                                    </a>
                                </div>
                            </div>
                    </div>
                    <div class="col-md-7">
                        {{ str_limit(strip_tags($product['description']),2500,' (...)') }}
                    </div>
                </div>
            </div>
        </div>

    </section>

@endsection
