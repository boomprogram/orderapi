<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ระบบจัดการคำสั่งซื้อสินค้า MarketPlaces</title>
    @include('layouts.inc-stylesheets')
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="#"><b>ระบบจัดการคำสั่งซื้อสินค้า</b></a>
    </div>
    @include('flash_msg')
  <!-- /.login-logo -->
  <div class="login-box-body">
        <p class="login-box-msg">ลืมรหัสผ่าน</p>

        <form action="{{ route('password.email') }}" method="post">
            @csrf
          <div class="form-group has-feedback">
            <input type="email" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="row">
            <!-- /.col -->
            <div class="col-xs-12">
              <button type="submit" class="btn btn-warning btn-block btn-flat">ลืมรหัสผ่าน</button>
            </div>
            <!-- /.col -->
          </div>
        </form>

      </div>
      <!-- /.login-box-body -->
</div>
    @include('layouts.inc-scripts')
</body>
</html>


