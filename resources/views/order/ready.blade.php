@extends('layouts.layout')

@section('content')
@section('stylesheets')
<link rel="stylesheet" href="{{ asset('css/daterangepicker.css') }}">
<style>
.confirmWidth{
    width: 100%;
    margin: 0 auto;
    height: 100%;
}
        * {
		box-sizing: border-box;
		}
		.la-print{
		box-sizing: content-box;
		width: 700px;
		min-height: 990px;
		padding: 20px;
		border: 1px solid #ccc;
		margin: 0 auto 30px;
		}
		.pick-list{
			width: 700px
		}
		.pick-list .logo .logo-bg{
			width: 700px;
		}
		.pick-list .logo .logo-label{
			left: 0;
			top: 0;
			z-index: 2		
		}
		.pick-list .logo .logo_content{
			left: 0;
			top: 0;
			z-index: 3		
		}
		.pick-list .logo .logo_content .headline{
			padding-top: 85px;
			font-weight: 700;
			font-size: 13px;			
		}
		.pick-list table{
		    margin-top: 40px;
			width: 100%;
			font-size: 13px;
			border-collapse: collapse;
		}
		/* Create two equal columns that floats next to each other */
		.column {
		  float: left;
		  width: 100%;
		  padding: 10px;
		  height: 540px; /* Should be removed. Only for demonstration */
		}
		/* Clear floats after the columns */
		.row:after {
		  content: "";
		  display: table;
		  clear: both;
		}
		.box{
          border: solid 2px grey;
          padding-right: 30px;
          padding-bottom: 10px;
		}
		.text-address{
			width: 130px;
            padding-left: 8px;
            padding-bottom: 8px;
			color: white;
			background-color: grey;
			font-weight: bold;
            font-size: 14px;
		}
		.des{
			padding-left: 10px;
            font-size: 12px;
		}
        .col{
            float: left;
            width: 25%;
        }
        .modal-body { 
            max-height: 900px; 
            overflow-y: auto; 
        }

        In your stylesheet add:

        @media print {
        body * {
            visibility: hidden;
        }
        #section-to-print, #section-to-print * {
            visibility: visible;
        }
        #section-to-print {
            position: absolute;
            left: 0;
            top: 0;
        }
        }
</style>
@endsection
    <section class="content-header">
        <h1>
        ข้อมูลคำสั่งซื้อ
        </h1>
        <ol class="breadcrumb">
            <li class="active"><a href="#"><i class="fa fa-dashboard"></i> ข้อมูลคำสั่งซื้อ</a></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        @include('flash_msg')
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">รายการคำสั่งซื้อ</h3>
            </div>
            @php
                $countReady = App\Order::where('order_status', '=', 'READY_TO_SHIP')->count();
                $countComplete = App\Order::where('order_status', '=', 'COMPLETED')->count();
            @endphp
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li><a href="{{ route('order.view') }}">ทั้งหมด</a></li>
                  <li><a href="{{ route('order.unpaid') }}">Unpaid</a></li>
                  <li class="active"><a href="{{ route('order.ready') }}">พร้อมที่จะจัดส่ง ({{ $countReady }})</a></li>
                  <li><a href="{{ route('order.complete') }}">เสร็จสิ้น ({{ $countComplete }}) </a></li>
                </ul>
                  <!-- /.tab-pane -->
                  <div class="tab-content">
                        <div class="box box-success">
                                <div class="box-body">
                                    <form id="ready-search-form" action="{{ route('order.ready') }}" method="get">
                                        @csrf
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="ordersn" id="ordersn" placeholder="หมายเลขคำสั่งซื้อ" value="{{ old('ordersn') }}" autocomplete="off">
                                        </div>
                                        <div class="col-md-5">
                                            <select class="form-control" name="payments" id="payments" onchange="order_submit()">
                                                <option value="">--- วิธีการชำระเงิน ---</option>
                                                @foreach ($payments as $payment)
                                                    <option value="{{ $payment->payment_method }}">{{ $payment->payment_method }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control pull-right" name="date" id="date" placeholder="วันที่ได้รับชำระเงิน" autocomplete="off">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="item_sku" id="item_sku" placeholder="รหัสเก็บสินค้า" autocomplete="off">
                                        </div>
                                        <div class="col-md-2">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="name_customer" id="name_customer" placeholder="ลูกค้า" value="{{ old('name_customer') }}" autocomplete="off">
                                        </div>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" name="shipping" id="shipping" placeholder="ตัวเลือกในการจัดส่ง" autocomplete="off">
                                        </div>
                                        <div class="col-md-2">
                                            <button type="submit" class="btn btn-success">
                                                <span class="fa fa-search"> ค้นหา
                                            </button>
                                        </div>
                                </form>
                            </div>
                        </div>
                        <div class="box-body no-padding">
                            <div class="col-md-8">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-info btn-flat optionsready disabled dropdown-toggle" data-toggle="dropdown">
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" onclick="ship()"><i class="fa fa-ship"></i>นัดรับสินค้า</a></li>
                                        <li><a href="#" onclick="printOrder()"><i class="fa fa-print"></i>พิมพ์</a></li>
                                        <li><a href="#" onclick="filter()"><i class="fa fa-filter"></i>กรองคำสั่งซื้อ</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <form id="order-search-form" action="{{ route('order.ready') }}" method="get">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <select class="form-control" name="pagination" id="pagination" onchange="order_submit()">
                                            <option value="100" @if($pagination == 100) selected @endif>100</option>
                                            <option value="200" @if($pagination == 200) selected @endif>200</option>
                                        </select>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-1">

                            </div>
                        </div>
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%"><input type="checkbox" class="chk_allOrderReady"></th>
                                    <th width="8%">เอกสาร</th>
                                    <th>SKU <a href="{{ url('orders/ready') }}?sortby=item_sku&order=asc"><i class="fa fa-sort-asc"></i></a><a href="{{ url('orders/ready') }}?sortby=item_sku&order=desc"><i class="fa fa-sort-desc"></i></a></th>
                                    <th>หมายเลขคำสั่งซื้อ <a href="{{ url('orders/ready') }}?sortby=ordersn&order=asc"><i class="fa fa-sort-asc"></i></a><a href="{{ url('orders/ready') }}?sortby=ordersn&order=desc"><i class="fa fa-sort-desc"></i></a></th>
                                    <th width="12%">วันที่สั่งซื้อ <a href="{{ url('orders/ready') }}?sortby=date&order=asc"><i class="fa fa-sort-asc"></i></a><a href="{{ url('orders/ready') }}?sortby=date&order=desc"><i class="fa fa-sort-desc"></i></a></th>
                                    <th width="12%">ปรับปรุงวันที่</th>
                                    <th>วิธีการชำระเงิน</th>
                                    <th>ราคาขายปลีก</th>
                                    <th>จำนวนพิมพ์</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(count($orders) != 0)
                                <form class="form_allOrder" method="post">
                                @foreach ($orders as $ready)
                                <tr>
                                    <td><input type="checkbox" name="orderready_select" value="{{ $ready->ordersn }}"></td>
                                    <td>@if($ready->order_status != "CANCELLED" && $ready->order_status != "UNPAID") ใบแจ้งหนี้  @endif</td>
                                    <td>{{ $ready->item['item_sku'] }}</td>
                                    <td><a href="{{ route('order.show',$ready->ordersn) }}">{{ $ready->ordersn }} </a></td>
                                    <td>{{ ConvertDate($ready->created_at) }}</td>
                                    <td>{{ ConvertDate($ready->updated_at) }}</td>
                                    <td>@if($ready->payment_method == "Cash on Delivery") {{ $ready->payment_method }} @endif</td>
                                    <td>{{ number_format($ready->total_amount,2, '.', ',') }}</td>
                                    <td>พิมพ์ไปแล้ว {{ $ready->cum_printout }} ครั้ง</td>
                                </tr>
                                @endforeach
                                </form>
                            @endif
                            </tbody>
                        </table>
                    <div align="right">
                        {{ $orders->appends($_GET)->links() }}
                    </div>                
                  </div>

                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div>
              <!-- nav-tabs-custom -->
        </div>
    </section>

@endsection
@section('scripts')
<script src="{{ asset('js/moment.js') }}"></script>
<script src="{{ asset('js/daterangepicker.js') }}"></script>
<script src="{{ asset('js/JsBarcode.all.js')}}"></script>
<script>
    $('.chk_allOrderAll').on('click', function () {
        if($(this).is(':checked')) {
            $('input[name="orderall_select"]').prop('checked', true)
            $('.options').removeClass('disabled');
        }else {
            $('input[name="orderall_select"]').prop('checked', false)
            $('.options').addClass('disabled');
        }
    });
    $('.chk_allOrderUnpaid').on('click', function(){
        if($(this).is(':checked')) {
            $('input[name="orderunpaid_select"]').prop('checked', true)
            $('.optionsunpaid').removeClass('disabled');
        }else {
            $('input[name="orderunpaid_select"]').prop('checked', false)
            $('.optionsunpaid').addClass('disabled');
        }
    });
    $('.chk_allOrderReady').on('click', function(){
        if($(this).is(':checked')) {
            $('input[name="orderready_select"]').prop('checked', true)
            $('.optionsready').removeClass('disabled');
        }else {
            $('input[name="orderready_select"]').prop('checked', false)
            $('.optionsready').addClass('disabled');
        }
    });
    $('input[name="orderready_select"').on('click', function(){
        if($(this).is(':checked')){
            $('.optionsready').removeClass('disabled');
        }else{
            $('.optionsready').addClass('disabled');
        }
    });
    $('.chk_allOrdercomplate').on('click', function(){
        if($(this).is(':checked')){
            $('input[name="ordercomplate_select"]').prop('checked', true)
            $('.optionscomplate').removeClass('disabled');            
        }else{
            $('input[name="ordercomplate_select"]').prop('checked', false)
            $('.optionscomplate').addClass('disabled');
        }
    });
    $('input[name="ordercomplate_select"]').on('click', function(){
        if($(this).is(':checked')){
            $('.optionscomplate').removeClass('disabled');            
        }else{
            $('.optionscomplate').addClass('disabled');
        }
    });
    $('#date').daterangepicker({
      autoUpdateInput: false,
      locale: {
          cancelLabel: 'Clear'
      }
    });

    $('#date').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
    });

    function order_submit(){
        document.getElementById("ready-search-form").submit();
    }
    document.getElementById('pagination').onchange = function() { 
        window.location = "{!! $orders->url(1) !!}&pagination=" + this.value; 
    };
    function printOrder(orsenids){
        bootbox.confirm({
        message: "ต้องการสั่งพิมพ์ออเดอร์นี้ ?",
        buttons: {
            confirm: {
                label: 'ตกลง',
                className: 'btn-success'
            },
            cancel: {
                label: 'ยกเลิก',
                className: 'btn-danger'
            }
        },
        callback : function(result){
            if (result) {
                const arr = $(".form_allOrder").serializeArray();
                const ordersnIds = arr.map((item) => {
                    return  item.value
                });
                const count = $(".form_allOrder").serializeArray().length;
                $.ajax({
                    type: "POST",
                    url: "{{ route('order.updatePrint') }}",
                    data: { ordersn : ordersnIds,
                        _token: "{{ csrf_token() }}",
                    },
                    success: function(data){
                        var htmlPrint = '<center><button class="btn btn-primary" onclick="printDiv()">พิมพ์</button><button class="btn btn-primary" onclick="closePrint()">ปิดหน้าต่าง</button></center>';
                            htmlPrint += '<div id="printableArea">';
                            htmlPrint += '<div class="la-print">';
                            htmlPrint += '<div class="pick-list"><div class="logo"><img class="logo-bg" src="{{ asset("img/print_logo_bg.png") }}"><div class="logo-label"><img src="{{ asset("img/print_logo_label.png") }}"></div><div class="logo_content"><div class="headline">Picklist printed on: @php echo date("d M y");@endphp</div></div></div><table><tr><th>SKU</th><th>Image</th><th>Product</th><th>Order Number</th><th>Quantity</th></tr>';
                        $.each(data.order, function(i, orders) {
                            var returnedData = orders.item;
                            var returnedAddress = JSON.parse(orders.recipient_address);
                            var imgProduct = JSON.parse(orders.item.images);
                            htmlPrint += '<tr>';
                            htmlPrint += '<td>'+returnedData.item_sku+'</td>';
                            htmlPrint += '<td><img src='+imgProduct[0]+' width="100px" height="100px"></td>';
                            htmlPrint += '<td>'+returnedData.name+'</td>';
                            htmlPrint += '<td>'+orders.ordersn+'</td>';
                            htmlPrint += '<td align="center">'+orders.items.variation_quantity_purchased+'</td>';
                            htmlPrint += '<tr>';
                        });
                            htmlPrint += '</table>';
                            htmlPrint += '</div>';
                            htmlPrint += '</div>';
                            htmlPrint += '<div class="la-print" style="border: 0px solid #ccc">';
                        $.each(data.order, function(i,orders){
                            var returnedData = orders.item;
                            var returnedAddress = JSON.parse(orders.recipient_address);
                            var imgProduct = JSON.parse(orders.item.images);                          
                            if(orders.order_status == "COMPLETED" || orders.order_status == "READY_TO_SHIP"){
                                        htmlPrint += '<div class="column">';
                                        htmlPrint += '<table width="100%">';
                                        htmlPrint += '<tbody>';
                                        htmlPrint += '<tr>';
                                        htmlPrint += '<td width="35%"><img src="{{ asset("img/kerry_logo.jpg") }}" width="140px" height="60px"></td>';
                                        htmlPrint += '<td>AIRWAY BILL</td>';
                                        htmlPrint += '<td><center><div style=""><img src="{{ route('order.barcode') }}?codetype=Code39&size=40&text='+orders.tracking_no+'&print=true" alt="barcode" /></div></center></td>';
                                        htmlPrint += '</tr>';
                                        htmlPrint += '<tr>';
                                        htmlPrint += '<td colspan="2">';
                                        htmlPrint += '<div class="box"><div class="text-address">TO (ADDRESSEE) @php echo date("d M y");@endphp</div><div class="des">NAME : '+returnedAddress.name+'</div><div class="des">PHONE : '+returnedAddress.phone+'</div><div class="des">ADDRESS : '+returnedAddress.full_address+'</div>';
                                        htmlPrint += '</td>';
                                        htmlPrint += '<td>';
                                        htmlPrint += '<div class="box"><div class="text-address">FROM (SENDER) @php echo "Tag_'+orders.cum_printout+'";@endphp</div><div class="des">NAME : '+data.shop.shop_name+'</div><div class="des">ADDRESS : ร้านวัชรการไฟฟ้า 73 ซอยตากสิน 21 ถนนตากสิน สำเหร่ เขตธนบุรี กรุงเทพฯ 10600</div></div>';
                                        htmlPrint += '</td>';
                                        htmlPrint += '</tr>';
                                        htmlPrint += '<tr>';
                                        htmlPrint += '<td colspan="3">';
                                        htmlPrint += '<table width="100%" border="1">';
                                        htmlPrint += '<tr><th align="center">Productname</th>';
                                        htmlPrint += '<th align="center">Variable</th>';
                                        htmlPrint += '<th>SKU</th>';
                                        htmlPrint += '<th>Qty</th></tr>';
                                        htmlPrint += '<tr><td>'+returnedData.name+'</td>';
                                        htmlPrint += '<td>'+returnedData.item_sku+'</td>';
                                        htmlPrint += '<td>'+returnedData.item_sku+'</td>';
                                        htmlPrint += '<td align="center">'+orders.items.variation_quantity_purchased+'</td></tr>';
                                        htmlPrint += '</table>';
                                        htmlPrint += '</td>';
                                        htmlPrint += '</tr>';
                                        htmlPrint += '<tr>';
                                        htmlPrint += '<td width="30" colspan="2" valign="top">';
                                        htmlPrint += '<div class="box" style=""><div class="des"><strong>ORDER NO : </strong>'+orders.ordersn+'</div><div class="des"><strong>PICK UP DATE : </strong> @php echo date("d M y");@endphp</div><div class="des">ข้อความจากผู้ซื่้อ :<font color="green"><strong>'+returnedData.message_to_seller+'</strong></font></div>';
                                        htmlPrint += '</td>';
                                        htmlPrint += '<td width="25">';
                                        htmlPrint += '<div class="box" style="width:140px;"><div class="text-address" style="width:80px;">Payment</div><div class="des">'+orders.payment_method+'</div></div>';
                                        htmlPrint += '<img src="{{ asset("img/shopee-logo.png") }}" width="40px" height="55px">';
                                        htmlPrint += '</td>';
                                        htmlPrint += '</tr>';
                                        htmlPrint += '</tbody>';
                                        htmlPrint += '</table>';
                                        htmlPrint += '</div>';
                            }else{
                                alert("ไม่สามารถพิมพ์ใบปะหน้าสินค้าได้ เนื่องจากสถานะยังไม่สมบูรณ์ กรุณาลองใหม่อีกครั้ง");
                                window.location.reload();                                
                            }
                        });
                            htmlPrint += '</div>';
                            htmlPrint += '</div>';
                            htmlPrint += '</div>';
                            bootbox.dialog({ 
                                    message: ''+htmlPrint+'',
                                    closeButton: true,
                                    className: "large",
                            }).find("div.modal-dialog").addClass("confirmWidth"); 
                    }
                });
            }else{
                bootbox.hideAll();
                window.location.reload();
            }
        }
        });
    };
    function ship(){
        var ordersn = $('input[name="ordercomplate_select"]').val();
        bootbox.prompt({
            title: "นัดรับสินค้า"+ordersn,
            inputType: 'textarea',
            callback: function (result) {
                console.log(result);
            }
        });
    }
    function filter(){
        $.ajax({
            type: "GET",
            url: "{{ url('orders') }}?sortby=ordersn&order=desc",
            success: function(data){
                window.location.reload();
            }
        });
        
    }

    function LoadData(){
        var payments = $("#payments").val();
        $.ajax({
            type: "GET",
            url: "{{ route('order.view') }}/?payments="+ payments,
            success: function(data){
                $("table .allorder tbody").html(data);
                console.log(data);
            }
        })
    }

    function printDiv(){
        var printContents = document.getElementById("printableArea").innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }

    function closePrint(){
        window.location.reload();
    }
</script>
@endsection
