@extends('layouts.layout')

@section('content')

    <section class="content-header">
        <h1>
        รายละเอียดการสั่งซื้อหมายเลข {{$order->ordersn}}
        </h1>
        <ol class="breadcrumb">
            <li class="active"><a href="#"><i class="fa fa-dashboard"></i> ข้อมูลคำสั่งซื้อ</a></li>
        </ol>
    </section>
    @php
        $items = $order->items;
        $address = json_decode($order->recipient_address);
    @endphp
    <!-- Main content -->
    <section class="content">
        <div class="col-md-4">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">ข้อมูลของลูกค้า</h3>
                </div>
                <div class="box-body">
                    <table class="table">
                        <tr>
                            <th>วันที่</th>
                            <td>{{ ConvertDate1($order->created_at) }}</td>
                        </tr>
                        <tr>
                            <th>ลูกค้า</th>
                            <td>{{ $address->name }}</td>
                        </tr>
                        <tr>
                            <th>หมายเลขโทรศัพท์</th>
                            <td>{{ $address->phone }}</td>
                        </tr>
                        <tr>
                            <th>วิธีการชำระเงิน</th>
                            <td>{{ $order->payment_method }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">ข้อมูลการทำธุรกรรม</h3>
                </div>
                <div class="box-body">
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">ที่อยู่เรียกเก็บเงิน</h3>
                </div>
                <div class="box-body">
                    <p>{{ $address->name }}</p>
                    <p>{{ $address->full_address }}</p>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">ที่อยู่สำหรับจัดส่ง</h3>
                </div>
                <div class="box-body">
                    <p>{{ $address->name }}</p>
                    <p>{{ $address->full_address }}</p>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">สินค้า</h3>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>รหัสสินค้า</th>
                            <th>รหัสเก็บสินค้า</th>
                            <th>สินค้า</th>
                            <th>ประเภทของการจัดส่ง</th>
                            <th>ราคาขายปลีก</th>
                            <th>การคืนเงิน</th>
                            <th>Bundle ID</th>
                            <th>Bundle Discount</th>
                            <th>Voucher ID</th>
                            <th>Voucher Discount</th>
                            <th>การจัดส่ง</th>
                            <th>สถานะ</th>
                        </tr>
                    <tbody>
                        <tr>
                            <td>{{ $order->item->item_id }}</td>
                            <td>{{ $order->item->item_sku }}</td>
                            <td>{{ $order->item->name }}</td>
                            <td>{{ $order->shipping_carrier }}</td>
                            <td>{{ number_format($order->item->variation_original_price,2,'.',',') }}</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>{{ $order->actual_shipping_cost }}</td>
                            <td>{{ $order->order_status }}</td>
                        </tr>
                        <tr>
                            <th colspan="8" style="font-size:18px;text-align:right;">จำนวนรวมทั้งหมด</th>
                            <th colspan="4" style="font-size:18px;text-align:center;">{{ number_format($order->total_amount,2,'.',',') }} บาท</th>
                        </tr>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

@endsection
