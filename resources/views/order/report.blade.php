<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Report</title>
    <style>
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ asset('fonts/THSarabunNew.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: bold;
            src: url("{{ asset('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: normal;
            src: url("{{ asset('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: bold;
            src: url("{{ asset('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
        }
        body {
            font-family: "THSarabunNew";
        }
    </style>
</head>
<body>
    <h1>รายงานสินค้า </h1>
    <h2>ตั้งแต่วันที่ {{ ConvertDate1($start_date) }} ถึงวันที่ {{ ConvertDate1($end_date) }}</h2>
    <table border="1">
        <tr>
            <th>SKU</th>
            <th>Product name</th>
            <th>Seles</th>
            <th>Result</th>
        </tr>
        @foreach ($reports as $report)
        <tr>
            <th>{{ $report->item['item_sku'] }}</th>
            <th>{{ $report->item['name'] }}</th>
            <th>{{ $count }}</th>
            <th>{{ $report->item['sales'] }}</th>
        </tr>            
        @endforeach
    </table>
</body>
</html>