<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>SellerCenter</title>
    <style>
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ asset('fonts/THSarabunNew.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: bold;
            src: url("{{ asset('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: normal;
            src: url("{{ asset('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: bold;
            src: url("{{ asset('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
        }
        body {
            font-family: "THSarabunNew";
        }
        .page-break {
            page-break-after: always;
        }
        span.text-dt {
            border-bottom: 1px dotted #000;
            text-decoration: none;
            width:100px;
            height:100px;
        }
        .line{
            margin: 10 0 10 0px;
            border-bottom: 1px dotted #000;
        }
        .linecolor{
            border: 7px solid #FAEBD7;
        }
		.box{
          border: solid 2px grey;
          padding-right: 30px;
          padding-bottom: 10px;
		}
		.text-address{
			width: 130px;
            padding-left: 8px;
            padding-bottom: 8px;
			color: white;
			background-color: grey;
			font-weight: bold;
            font-size: 14px;
		}
		.des{
			padding-left: 10px;
            font-size: 12px;
		}
        .col{
            float: left;
            width: 50%;
        }
        .Table
        {
            display: table;
            width: 100%;
        }
        .Title
        {
            display: table-caption;
            text-align: center;
            font-weight: bold;
            font-size: larger;
        }
        .Heading
        {
            display: table-row;
            font-weight: bold;
            text-align: center;
        }
        .Row
        {
            display: table-row;
        }
        .Cell
        {
            display: table-cell;
            border: solid;
            border-width: thin;
            padding-left: 5px;
            padding-right: 5px;
        }
    </style>
</head>
<body>
    <div class="linecolor"></div>
    @php
        echo "<font size='70px'><strong>SELLER</strong>CENTER</font>";
        $item = json_decode($order->items);
        $address = json_decode($order->recipient_address);
        $getImg = json_decode($image);
    @endphp
        <br><font size='20px'>Checklist printed on: @php echo date('d M Y') @endphp</font>
    <hr>
    <table width="100%" style="border:1px solid #000000;font-size:14pt;">
        <tr align="center">
            <th>SKU</th>
            <th>Image</th>
            <th>Product</th>
            <th>Order Number</th>
            <th>Quantity</th>
        </tr>
        <tr>
            <td>{{ $item->item_sku }}</td>
            <td><img src="{{ $getImg[0] }}" width="100px" height="100px"></td>
            <td>{{ $item->item_name }}</td>
            <td>{{ $order->ordersn }}</td>
            <td align="center">{{ $item->variation_quantity_purchased }}</td>
        </tr>
    </table>

    <div class="page-break"></div>

    @php
        $border = 2;//กำหนดความหน้าของเส้น Barcode
        $height = 40;//กำหนดความสูงของ Barcode
        $generatorHTML = new Picqer\Barcode\BarcodeGeneratorHTML();
    @endphp

        <table width="">
             <tbody>
                <tr>
                    <td rowspan="2">
                        <div class="box">
                            <div class="text-address">TO (ADDRESSEE) <?php echo date('d M y');?></div>
                            <div class="des">NAME : {{ $address->name }}</div>
                            <div class="des">PHONE : {{ $address->phone }}</div>
                            <div class="des">ADDRESS : {{ $address->full_address }}
                        </div>
                    </td>
                    <td width="50">@php echo "<center><div style=''>".$generator->getBarcode($order->tracking_no , $generator::TYPE_CODE_128,$border,$height)."</div></center>"; @endphp @php echo "<center>".$order->tracking_no."</center>"; @endphp</td>
                </tr>
                    <tr>
                        <td>
                            <div class="box">
                                <div class="text-address">FROM (SENDER) <?php echo "Tag_001/100";?></div>
                                <div class="des">NAME : {{ $shop->shop_name }}</div>
                                <div class="des">ADDRESS : ร้านวัชรการไฟฟ้า 73 ซอยตากสิน 21 ถนนตากสิน สำเหร่ เขตธนบุรี กรุงเทพฯ 10600</div>
                            </div>			
                        </td>
                    </tr>
                </tbody>
        </table>
            <table width="100%">
                <tbody>
                    <tr>
                        <td rowspan="4" valign="top" width="430">
                                    <div class="Table">
                                        <div class="Heading">
                                            <div class="Cell">
                                                SKU
                                            </div>
                                            <div class="Cell">
                                                Variable
                                            </div>
                                            <div class="Cell">
                                                Quantity
                                            </div>
                                        </div>
                                        <div class="Row">
                                            <div class="Cell">
                                                <p>{{ $item->item_sku }}</p>
                                            </div>
                                            <div class="Cell">
                                                <p>{{ $item->item_name }}</p>
                                            </div>
                                            <div class="Cell">
                                                <p><center>{{ $item->variation_quantity_purchased }}</center></p>
                                            </div>
                                        </div>
                                    </div>
                                    @php
                                        if($order->message_to_seller != NULL)
                                            $message = $order->message_to_seller;
                                        else{
                                            $message = "";
                                        }
                                    @endphp
                                        <center>ข้อความจากผู้ซื่้อ :<font color="green"><strong>{{ $message }}</strong></font></center>
                        </td>
                        <td>
                            <div class="box" style="margin-left:10px;width:100px;">
                                <div class="text-address" style="width:80px;">Payment</div>
                                <div class="des">{{ $order->payment_method }}</div>
                            </div>		
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <img style="" src="data:image/png;base64, @php echo $qr_image; @endphp"><br><div style="margin-top:-20px;">{{ $order->ordersn }}</div>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <br>
                                        <img src="{{ asset('img/shopee-logo.png') }}" width="40px" height="55px">
                                        <img src="{{ asset('img/nologo.png') }}" width="50px" height="50px">
                                    </td>
                                </tr>
                            </tbody>
                            </table>
                
            </td>
    </table>
</body>
</html>