@extends('layouts.layout')

@section('content')
    <section class="content-header">
        <h1>
        ข้อมูลร้านค้า
        </h1>
        <ol class="breadcrumb">
            <li class="active"><a href="#"><i class="fa fa-dashboard"></i> ข้อมูลร้านค้า</a></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box box-default">
            <div class="box-body">
                <form class="form-horizontal" action="" method="post">
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">ShopID</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="shop_id" id="shop_id" value="{{ $shop->shop_id }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">ShopName</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="shop_name" id="shop_name" value="{{ $shop->shop_name }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Country</label>
                        <div class="col-sm-1">
                            <input type="text" class="form-control" name="country" id="country" value="{{ $shop->country }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-5">
                            <textarea id="editor1" name="editor1" rows="10" cols="100">{{ $shop->shop_description }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Image</label>
                        <div class="col-sm-3">
                            <input type="file" name="images" id="images">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Itemlimit</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="item_limit" id="item_limit" value="{{ $shop->item_limit }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Status</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="status" id="status" value="{{ $shop->status }}">
                        </div>
                    </div>

                </form>
            </div>
            <div class="box-footer">
                <center><button type="submit" class="btn btn-success">แก้ไขข้อมูล</button></center>
            </div>
        </div>
    </section>

@endsection
@section('scripts')
<script>
$(function () {
    CKEDITOR.replace('editor1');
    $(".textarea").wysihtml5();
});
</script>
@endsection