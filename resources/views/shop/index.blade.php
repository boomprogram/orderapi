@extends('layouts.layout')

@section('content')

    <section class="content-header">
        <h1>
        ข้อมูลร้านค้า
        </h1>
        <ol class="breadcrumb">
            <li class="active"><a href="#"><i class="fa fa-dashboard"></i> ข้อมูลร้านค้า</a></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">ข้อมูลร้านค้า</h3>
            </div>
            <div class="box-body">
                <table class="table">
                    <tr>
                        <th>ShopID</th>
                        <th>ShopName</th>
                        <th>Country</th>
                        <th>Status</th>
                        <th>Item_limit</th>
                        <th>&nbsp;</th>
                    </tr>
                <tbody>
                    @foreach($shops as $shop)
                    <tr>
                        <td>{{ $shop->shop_id }}</td>
                        <td>{{ $shop->shop_name }}</td>
                        <td>{{ $shop->country }}</td>
                        <td>{{ $shop->status }}</td>
                        <td>{{ $shop->item_limit }}</td>
                        <td><a href="{{ route('shop.edit', $shop->shop_id) }}" class="btn-sm btn-primary"><i class="fa fa-pencil"></i></a></td>
                    </tr>
                    @endforeach
                </tbody>
                </table>
                
            </div>
        </div>
    </section>

@endsection
