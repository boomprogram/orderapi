@extends('layouts.layout')

@section('content')

    <section class="content-header">
        <h1>
        ข้อมูลหมวดสินค้า
        </h1>
        <ol class="breadcrumb">
            <li class="active"><a href="#"><i class="fa fa-dashboard"></i> ข้อมูลหมวดสินค้า</a></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">หมวดสินค้า</h3>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th width="10%">#</th>
                            <th>รายการ</th>
                            {{-- <th>&nbsp;</th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($categories as $key => $category)
                            <tr>
                                <td>{{ $key+$categories->firstItem() }}</td>
                                <td>{{ $category->category_name }}</td>
                                {{-- <td><a href="{{ route('product.show', $value["item_id"])}}" class="btn-sm btn-primary"></a></td> --}}
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $categories->links() }}
            </div>
        </div>
    </section>

@endsection
