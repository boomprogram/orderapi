@extends('layouts.layout')

@section('content')
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
            ข้อมูลรวม
            </h1>
            <ol class="breadcrumb">
                <li class="active"><a href="#"><i class="fa fa-dashboard"></i> ข้อมูลรวม</a></li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">ข้อมูลการสั่งซื้อล่าสุด</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>รายการสั่งซื้อ</th>
                        <th>สถานะการสั่งซื้อ</th>
                        <th>ผู้สั่งซื้อ</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($orders as $order)
                        <tr>
                            <td><a href="{{ route('order.show',$order->ordersn) }}">{{ $order->ordersn }} </a></td>
                            @php
                                $address = json_decode($order->recipient_address);
                                //
                                $label_color;
                                if($order->order_status == "UNPAID"){
                                    $label_color = 'info';
                                }elseif($order->order_status == 'READY_TO_SHIP'){
                                    $label_color = 'warning';
                                }elseif($order->order_status == 'COMPLETED'){
                                    $label_color = 'success';
                                }elseif($order->order_status == 'IN_CANCEL'){
                                    $label_color = 'warning';
                                }elseif($order->order_status == 'CANCELLED'){
                                    $label_color = 'danger';
                                }elseif($order->order_status == 'TO_RETURN'){
                                    $label_color = 'warning';
                                }
                                //
                            @endphp
                            <td>{{ $order->item->name }}</td>
                            <td><span class="label label-{{ $label_color }}">{{ $order->order_status }}</span></td>
                            <td>
                            <div class="sparkbar" data-color="#00a65a" data-height="20">{{ $address->name }}</div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </section>
@endsection
