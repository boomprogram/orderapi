
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif


@if (\Session::has('success'))
<div class="alert alert-success">
    <i class="fa fa-check-circle"></i> {!! \Session::get('success') !!}
</div>
@endif

@if (\Session::has('error'))
<div class="alert alert-danger">
    <i class="fa fa-close"></i> {!! \Session::get('error') !!}
</div>
@endif
