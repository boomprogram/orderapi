<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', function () {
    return view('auth.login');
});
Route::get('/forgot', function () {
    return view('auth.passwords.email');
});

Auth::routes();

Route::group(['middleware' => ['auth']], function () {

Route::get('/', 'HomeController@index')->name('home');
//Category//
Route::group(['prefix' => 'categories', 'as' => 'category.'], function () {
    Route::get('/', 'CategoryController@index')->name('view');
});
//Product//
Route::group(['prefix' => 'products', 'as' => 'product.'], function () {
    Route::get('/', 'ProductController@index')->name('view');
    Route::get('/create', 'ProductController@create')->name('create');
    Route::post('/save', 'ProductController@store')->name('save');
    Route::get('/show/{id}', 'ProductController@show')->name('show');
});
//Order//
Route::group(['prefix' => 'orders', 'as' => 'order.'], function () {
    Route::get('/', 'OrderController@index')->name('view');
    Route::get('/unpaid', 'OrderController@dataUnpaid')->name('unpaid');
    Route::get('/ready', 'OrderController@dataReady')->name('ready');
    Route::get('/complete', 'OrderController@dataComplete')->name('complete');
    Route::get('/show/{id}', 'OrderController@show')->name('show');
    Route::get('/exportorderall/{id}', 'OrderController@exportOrderAll')->name('exportorderall');
    Route::get('/exportdiscountitem/{id}', 'OrderController@exportDiscountItem')->name('exportdiscountitem');
    Route::post('/updatePrint', 'OrderController@updateCountPrint')->name('updatePrint');
    Route::get('/dataOrderAjax', 'OrderController@DataOrderAll')->name('dataAll');
    Route::get('/getbarcode', 'OrderController@barcode')->name('barcode');
    Route::get('/tagsgroup', 'OrderController@barcode')->name('tagsgroup');
    Route::post('/report', 'OrderController@report')->name('report');
});
//User//
Route::group(['prefix' => 'users', 'as' => 'user.'], function () {
    Route::get('/', 'UserController@index')->name('view');
    Route::get('/create', 'UserController@create')->name('create');
    Route::post('/save', 'UserController@store')->name('save');
    Route::get('/edit/{id}', 'UserController@edit')->name('edit');
    Route::post('/update/{id}', 'UserController@update')->name('update');
    Route::post('/disabled', 'UserController@disabled')->name('disabled');
    Route::post('/enabled', 'UserController@enabled')->name('enabled');
    Route::get('/profile/{id}', 'UserController@editProfile')->name('editProfile');
    Route::get('/updateprofile/{id}', 'UserController@updateProfile')->name('updateProfile');
});
//Config//
Route::group(['prefix' => 'configs', 'as' => 'config.'], function () {
    Route::get('/', 'ConfigController@index')->name('view');
    Route::post('/syncdata', 'ConfigController@syncdata')->name('syncdata');
    Route::get('/testdata', 'ConfigController@testdata')->name('testdata');
});
//Shop//
Route::group(['prefix' => 'shops', 'as' => 'shop.'], function () {
    Route::get('/', 'ShopController@index')->name('view');
    Route::get('/edit/{id}', 'ShopController@edit')->name('edit');
});

});
